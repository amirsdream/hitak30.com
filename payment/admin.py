from django.contrib import admin

# Register your models here.
from .models import PaymentTransaction


class PaymentTransactionAdmin(admin.ModelAdmin):
    class Meta:
        model = PaymentTransaction



admin.site.register(PaymentTransaction, PaymentTransactionAdmin)