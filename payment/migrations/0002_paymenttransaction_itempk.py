# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-01-18 00:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymenttransaction',
            name='itempk',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
