# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-04-14 18:00
from __future__ import unicode_literals

import datetime
from django.db import migrations
import django_jalali.db.models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0004_remove_paymenttransaction_transaction_date_jalali'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymenttransaction',
            name='transaction_date_jalali',
            field=django_jalali.db.models.jDateField(default=datetime.datetime.now),
        ),
    ]
