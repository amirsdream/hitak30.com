from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^payment/callback/$', views.callback, name='callback'),
    url(r'^view/$', views.view_payments, name='view_payments'),
    url(r'^report_pay/$', views.report_pay, name='report_pay'),
    url(r'^export_list/$', views.report_list,
        name='report_list'),
    url(r'^pay/(?P<pk>\d+)/$', views.set_negotiation_for_payment, name='pay'),
]
