# -*- coding: utf-8 -*-

try:
    from django.contrib.contenttypes import fields as generic
except ImportError:
    from django.contrib.contenttypes import generic

from datetime import datetime
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django_jalali.db import models as jmodels


class PaymentTransaction(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    amount = models.PositiveIntegerField()
    transaction_date = models.DateTimeField(auto_now_add=True)
    transaction_date_jalali = jmodels.jDateField(default=datetime.now)
    success = models.BooleanField(default=False)
    error_code = models.IntegerField(default=0)
    error_description = models.CharField(max_length=300, blank=True,
                                         default='درخواست شروع پرداخت برای بانک ارسال شده است.')

    order_id = models.BigIntegerField(default=0)
    itempk = models.IntegerField(null=True, blank=True)
    RefId = models.CharField(max_length=40, null=True, blank=True)
    ResCode = models.IntegerField(null=True, blank=True)
    SaleOrderId = models.CharField(max_length=40, null=True, blank=True)
    SaleReferenceId = models.CharField(max_length=40, null=True, blank=True)

    item_content_type = models.ForeignKey(
        ContentType,
        verbose_name='content page',
        null=True,
        blank=True,
    )
    item_object_id = models.PositiveIntegerField(
        verbose_name='related object',
        null=True,
    )
    content_object = generic.GenericForeignKey('item_content_type', 'item_object_id')

    def __unicode__(self):
        return 'خرید برای کاربر %s,s%' % (self.user.username, self.date)

    class Meta:
        db_table = 'payment_transaction'
