# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-06-08 21:13
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('institute', '0015_auto_20180525_1138'),
    ]

    operations = [
        migrations.AddField(
            model_name='institute',
            name='bankaccnum',
            field=models.PositiveIntegerField(default=0, validators=[django.core.validators.MaxValueValidator(9999999999)]),
        ),
    ]
