# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-06-26 10:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('institute', '0017_paymentconfirm'),
    ]

    operations = [
        migrations.AddField(
            model_name='institute',
            name='status',
            field=models.BooleanField(default=True),
        ),
    ]
