# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-05-10 20:25
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('institute', '0012_auto_20180508_1351'),
    ]

    operations = [
        migrations.CreateModel(
            name='InstituteStudent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('institute', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='instituteservice', to='institute.Institute')),
                ('students', models.ManyToManyField(related_name='institutestudent', to=settings.AUTH_USER_MODEL)),
                ('term', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='terminstitute', to='institute.Term')),
            ],
        ),
        migrations.AddField(
            model_name='institute',
            name='institutestudent',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='institutestudent', to='institute.InstituteStudent'),
        ),
    ]
