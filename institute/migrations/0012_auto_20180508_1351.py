# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-05-08 09:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('institute', '0011_auto_20180508_0919'),
    ]

    operations = [
        migrations.AddField(
            model_name='driverstudent',
            name='term',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='term', to='institute.Term'),
        ),
        migrations.RemoveField(
            model_name='term',
            name='driverstudent',
        ),
        migrations.AddField(
            model_name='term',
            name='driverstudent',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='driverstudent', to='institute.DriverStudent'),
        ),
    ]
