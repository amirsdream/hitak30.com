# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-04-17 18:00
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('institute', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='institute',
            old_name='adress',
            new_name='address',
        ),
    ]
