from django.conf.urls import url

from . import views

#
urlpatterns = [
    url(r'^institutereg/$', views.institutereg, name='institutereg'),
    url(r'^paymentgroup/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.paymentgroup, name='paymentgroup'),
    url(r'^paymentgroupterm/$', views.paymentgroupterm, name='paymentgroupterm'),
    url(r'^institutelist/$', views.institutelist, name='institutelist'),
    url(r'^instituteedit/(?P<pk>\d+)/$', views.instituteedit, name='instituteedit'),
    url(r'^institutedel/(?P<pk>\d+)/$', views.institutedel, name='institutedel'),
    url(r'^termreg/$', views.termreg, name='termreg'),
    url(r'^termlist/$', views.termlist, name='termlist'),
    url(r'^termedit/(?P<pk>\d+)/$', views.termedit, name='termedit'),
    url(r'^termdel/(?P<pk>\d+)/$', views.termdel, name='termdel'),
    url(r'^selectterm/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.selectterm, name='selectterm'),
    url(r'^transferfund/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.transferfund, name='transferfund'),
    url(r'^transferfundcancel/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.transferfundcancel, name='transferfundcancel'),
    url(r'^paymentconfirm/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.paymentconf, name='paymentconfirm'),
    url(r'^selectaccountingterm/$', views.selectaccountingterm, name='selectaccountingterm'),
    url(r'^selectaccountingmanagerterm/$', views.selectaccountingmanagerterm, name='selectaccountingmanagerterm'),
    url(r'^selectaccountingmanagerserviceterm/$', views.selectaccountingmanagerserviceterm, name='selectaccountingmanagerserviceterm'),
    url(r'^newtermdriver/$', views.newtermdriver, name='newtermdriver'),
    url(r'^studenttermselect/$', views.studenttermselect, name='studenttermselect'),
]
