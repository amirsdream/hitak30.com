from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404, HttpResponse

from registeration.models import Profile
from .forms import InstituteForm, TermForm, PaymentConfirmForm
from .models import Institute, Term, PaymentConfirm
from driver.models import TotalSalary
from django.contrib import messages


@login_required()
def institutereg(request):
    if request.method == "POST":
        form = InstituteForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('institutelist')
    else:
        form = InstituteForm()
    return render(request, 'instituteadd.html', {'form': form})


# @login_required()
# def instituteshow(request, pk):
#     comp = get_object_or_404(Institute, pk=pk)
#     return render(request, 'complaintshow.html', {'comp': comp})

@login_required()
def institutelist(request):
    ins = Institute.objects.all()
    return render(request, 'institutelist.html', {'profile': ins})


@login_required()
def institutedel(request, pk):
    comp = get_object_or_404(Institute, pk=pk)
    comp.delete()
    return redirect('institutelist')


@login_required()
def instituteedit(request, pk):
    # user1 = get_object_or_404(User, pk=pk)
    comp = get_object_or_404(Institute, pk=pk)
    if request.method == 'POST':
        # form = SignUpForm(request.POST, instance=user1)
        form = InstituteForm(request.POST, instance=comp)
        if form.is_valid():
            poll = form.save()
            poll.save()
            return redirect('institutelist')

    else:
        # form = SignUpForm(instance=user1)
        form = InstituteForm(instance=comp)

    return render(request, 'instituteadd.html', {'form': form})


@login_required()
def termlist(request):
    ins = Term.objects.all()
    return render(request, 'termlist.html', {'profile': ins})


@login_required()
def termdel(request, pk):
    comp = get_object_or_404(Term, pk=pk)
    comp.delete()
    return redirect('termlist')


@login_required()
def termedit(request, pk):
    # user1 = get_object_or_404(User, pk=pk)
    comp = get_object_or_404(Term, pk=pk)
    if request.method == 'POST':
        # form = SignUpForm(request.POST, instance=user1)
        form = TermForm(request.POST, instance=comp)
        if form.is_valid():
            poll = form.save()
            if request.POST.get('thu') != "on":
                poll.status = False
            else:
                poll.status = True
            print(request.POST.get('thu'))
            poll.save()
            return redirect('termlist')

    else:
        # form = SignUpForm(instance=user1)
        form = TermForm(instance=comp)
    return render(request, 'termedit.html', {'form': form, 'comp': comp})


@login_required()
def termreg(request):
    profile = Profile.objects.filter(Q(role='driver') | Q(role='manager'))
    if request.method == "POST":
        form = TermForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            for pro in profile:
                u = User.objects.get(id=pro.id)
                print(post.id)
                post.driver.add(u)
                post.save()
            print(post.driver.all())
            return redirect('termlist')
    else:
        form = TermForm()
    return render(request, 'termadd.html', {'form': form})


@login_required()
def selectterm(request, slug):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        return redirect('driverlist', slug=b)

    return render(request, 'termselect.html', {'term': term})


@login_required()
def selectaccountingterm(request):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        return redirect('driveraccountinglist', slug=b)

    return render(request, 'termselect.html', {'term': term})


@login_required()
def selectaccountingmanagerterm(request):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        return redirect('manageraccountinglist', slug=b)

    return render(request, 'termselect.html', {'term': term})


@login_required()
def selectaccountingmanagerserviceterm(request):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        return redirect('servicemanageraccountinglist', slug=b)

    return render(request, 'termselect.html', {'term': term})


@login_required()
def newtermdriver(request):
    profile = Profile.objects.filter(Q(role='driver') | Q(role='manager') | Q(role='manager service')).filter(
        disable=False)
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        c = request.POST['my_options3']
        print(c)
        u = User.objects.get(username=c)
        comp = get_object_or_404(Term, name=b)
        try:
            comp.driver.add(u)
            comp.save()
        except:
            print('except')

        print('ok')
        # for pro in profile:
        #     u = User.objects.get(id=pro.id)
        #     print(term)
        #     comp.driver.add(u)
        #     comp.save()
        return redirect('driverindex')

    return render(request, 'termselectdriver.html', {'term': term, 'profile': profile})


@login_required()
def studenttermselect(request):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        comp = get_object_or_404(Term, name=b)
        print('ok')
        return redirect('studentuserlistterm', slug=b)

    return render(request, 'studenttermselect.html', {'term': term})


@login_required()
def paymentgroupterm(request):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        comp = get_object_or_404(Term, name=b)
        print('ok')
        return redirect('calclist', slug=b)

    return render(request, 'studenttermselect.html', {'term': term})


@login_required()
def paymentgroup(request, slug):
    term = Term.objects.filter(name=slug)[0]
    pro = term.driver.all()
    paymentconfirm = PaymentConfirm.objects.filter(term=term)
    profile = []
    confirm = False
    for p in paymentconfirm:
        if p.status:
            confirm = p.status
    for p in pro:
        profile.append(get_object_or_404(Profile, user=p))

    return render(request, 'paymentgroup.html', {'profile': profile, 'slug': slug, 'confirm': confirm})


@login_required()
def transferfund(request, slug):
    profile = Profile.objects.filter(Q(role='driver') | Q(role='manager') | Q(role='manager service'))
    term = Term.objects.filter(name=slug)
    ins = Institute.objects.all()
    # profile = []
    x = []
    a = '0000000000'
    b = '00000000000000000'
    try:
        for p in profile:
            if p.bankaccname == "mellat":
                a = p.bankaccnum
            try:
                tsalary = TotalSalary.objects.filter(user=p.user).filter(term=term[0])
                ttsalary = 0
                help = 0
                bhelp = 0
                for i in tsalary:
                    if i.status!="True":
                        ttsalary += i.totalsalary
                if ttsalary:
                    help = ttsalary + 1000000000000000
                    bhelp = int(a) + 10000000000
                    x.append(str(bhelp)[1:11] + (str(help))[1:16] + b + '                             ' + '\n')
                    for i in tsalary:
                        i.status = "progress"
                        i.save()
                # print(a)
            except Exception as e:
                s = str(e)
                print(s)
        a = '0000000000'
        b = '00000000000000000'
        for p in ins:
            if p.bankaccnum:
                a = p.bankaccnum
            try:

                tsalary = TotalSalary.objects.filter(institute=p).filter(term=term[0])
                ttsalary = 0
                help = 0
                bhelp = 0
                for i in tsalary:
                    if i.status != "True":
                        ttsalary += i.totalsalary
                if ttsalary:
                    help = ttsalary + 1000000000000000
                    bhelp = int(a) + 10000000000
                    x.append(str(bhelp)[1:11] + str(help)[1:16] + b + '                             ' + '\n')
                    for i in tsalary:
                        i.status = "progress"
                        i.save()
                # print(a)
            except Exception as e:
                s = str(e)
                print(s)
    except:
        print('ok')
    # return render(request, 'output.txt', {'profile':profile}, content_type="text/plain")
    # messages.info(request, 'تمامی حواله ها ارسال شد')
    filename = "ELFL.txt"
    content = x
    response = HttpResponse(content, content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename={0}'.format(filename)
    return response


@login_required()
def transferfundcancel(request, slug):
    term = Term.objects.filter(name=slug)
    tsalary = TotalSalary.objects.filter(term=term[0])
    for ts in tsalary:
        if ts.status!="True":
            ts.status = False
            ts.save()
    print('cancel')
    messages.info(request, 'تمامی حواله ها لغو شد')
    return redirect('calclist', slug=slug)


@login_required()
def paymentconf(request, slug):
    term = Term.objects.filter(name=slug)
    tsalary = TotalSalary.objects.filter(term=term[0])
    if request.method == 'POST':
        if request.POST:
            a = request.POST['my_options1']
            for i in tsalary:
                if i.institute:
                    if i.status == "progress":
                        ins = get_object_or_404(Institute, pk=i.institute.pk)
                        tsalary = TotalSalary.objects.filter(institute=ins)
                        for t in tsalary:
                            t.receipt = a
                            t.status = "True"
                            t.save()
                else:
                    if i.status == "progress":
                        user = get_object_or_404(User, pk=i.user.pk)
                        tsalary = TotalSalary.objects.filter(user=user)
                        for t in tsalary:
                            t.receipt = a
                            t.status = "True"
                            t.save()
            return redirect('calclist', slug=slug)

    return render(request, 'paymentconfirm.html', {})
