from django.contrib.auth.models import User
from django.contrib.gis.db import models

from payment.models import PaymentTransaction
# from manager.models import Manager, ManagerService
from django.core.validators import MaxValueValidator

bankType = (
    ('mellat', 'ملت'),
    ('tejarat', u'تجارت'),
)
class Institute(models.Model):
    name = models.CharField(max_length=20)
    startpoint = models.PointField(blank=True, null=True)
    address = models.TextField()
    baseprice = models.DecimalField(max_digits=10, decimal_places=0, default=1350000)
    numberofsession = models.DecimalField(max_digits=10, decimal_places=0, default=1)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    # bankaccnum = models.PositiveIntegerField(validators=[MaxValueValidator(9999999999)], default=0000000000)
    institutestudent = models.ForeignKey('InstituteStudent', related_name='institutestudent', null=True)
    status = models.BooleanField(default=True)
    bankaccnum = models.PositiveIntegerField(validators=[MaxValueValidator(9999999999)], default=0000000000, null=True)
    bankaccname = models.CharField(max_length=100, default="mellat", choices=bankType,null=True)
    # student = models.ManyToManyField(User, related_name='studentins')
    # driver = models.ManyToManyField(User, related_name='driverins')
    # driverstudent = models.ForeignKey('DriverStudent', related_name='driverstudent',null=True)
    # managerdriver = models.ForeignKey('ManagerDriver', related_name='managerdriver',null=True)
    #
    def __str__(self):
        return u'{0}'.format(self.name)


class Term(models.Model):
    name = models.CharField(max_length=20)
    status = models.BooleanField(default=True)
    student = models.ManyToManyField(User, related_name='student')
    driver = models.ManyToManyField(User, related_name='driver')
    manager = models.ManyToManyField(User, related_name='termmanager')
    managerservice = models.ManyToManyField(User, related_name='termmanagerservice')
    payment = models.ManyToManyField(PaymentTransaction)

    # managerservice =models.ManyToManyField(User, related_name='managerservice')
    # manager =models.ManyToManyField(User, related_name='manager')
    # driverstudent = models.ForeignKey('DriverStudent', related_name='driverstudent', null=True)
    # mangerservicedriver = models.ForeignKey(ManagerService, related_name='managerservicedriver', null=True)
    # manger = models.ForeignKey(Manager, related_name='managerdriver', null=True)

    def __str__(self):
        return u'{0}'.format(self.name)


class DriverStudent(models.Model):
    students = models.ManyToManyField(User, related_name='studentservice')
    driver = models.ForeignKey(User, related_name='driverservice', null=True)
    term = models.ForeignKey('Term', related_name='term', null=True)

    @classmethod
    def make_student(cls, driver, term, new_student):
        driverstudent, created = cls.objects.get_or_create(
            driver=driver,
            term=term,
        )
        driverstudent.students.add(new_student)

    @classmethod
    def loose_student(cls, driver, term, new_student):
        driverstudent, created = cls.objects.get_or_create(
            driver=driver,
            term=term,
        )
        driverstudent.students.remove(new_student)


class InstituteStudent(models.Model):
    students = models.ManyToManyField(User, related_name='institutestudent')
    institute = models.ForeignKey('Institute', related_name='instituteservice', null=True)
    term = models.ForeignKey('Term', related_name='terminstitute', null=True)

    @classmethod
    def make_student(cls, institute, term, new_student):
        institutestudent, created = cls.objects.get_or_create(
            institute=institute,
            term=term,
        )
        institutestudent.students.add(new_student)

    @classmethod
    def loose_student(cls, institute, term, new_student):
        institutestudent, created = cls.objects.get_or_create(
            institute=institute,
            term=term,
        )
        institutestudent.students.remove(new_student)

class PaymentConfirm(models.Model):
    term = models.ForeignKey('Term', null=True)
    name = models.CharField(max_length=20)
    status = models.BooleanField(default=False)