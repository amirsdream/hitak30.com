from django import forms

from .models import Institute, Term, PaymentConfirm
from driver.models import TotalSalary
from mapwidgets.widgets import GooglePointFieldWidget


class InstituteForm(forms.ModelForm):
    class Meta:
        model = Institute
        fields = ('name', 'startpoint', 'address', 'baseprice', 'numberofsession', 'bankaccnum', 'bankaccname')
        widgets = {'startpoint': GooglePointFieldWidget, }
        labels = {
            'name': u'نام کانون',
            'startpoint': u'نقطه شروع',
            'address': u'آدرس',
            'baseprice': u'قیمت پایه',
            'numberofsession': u'تعداد جلسات',
            'bankaccnum': u'حساب بانکی',
            'bankaccname':u'تام بانک صاخب حساب'
        }

class TermForm(forms.ModelForm):
    class Meta:
        model = Term
        fields = ('name',)

        labels = {
            'name': u'نام',
            'status': u'وضعیت',
        }

class PaymentConfirmForm(forms.ModelForm):
    class Meta:
        model = PaymentConfirm
        fields = ('name',)

        labels = {
            'name': u'کد رهگیری',
            'status': u'وضعیت',
        }

class TsalaryConfirmForm(forms.ModelForm):
    class Meta:
        model = TotalSalary
        fields = ('receipt',)

        labels = {
            'receipt': u'کد رهگیری',
            'status': u'وضعیت',
        }