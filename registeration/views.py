import random
from datetime import datetime

import googlemaps
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404

from institute.models import Institute, Term, DriverStudent
# from .models import DriverProfile
from .forms import Form1, Formhandy, SignUpForm, ProfileForm, FactorForm, ComplaintForm, CancelDescForm ,SignUpForm2
from .models import Model1, Profile, Factor, Complaint


def auth_login(request):
    return render(request, 'login.html', )


@login_required()
def regnew(request):
    if request.user.is_staff:
        return render(request, 'manager.html')

    if request.method == "POST":
        form = Form1(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.sat = request.POST.get('sat', False)
            post.sun = request.POST.get('sun', False)
            post.mon = request.POST.get('mon', False)
            post.tue = request.POST.get('tue', False)
            post.wed = request.POST.get('wed', False)
            post.thu = request.POST.get('thu', False)
            post.fri = request.POST.get('fri', False)
            print(post.sun)
            post.save()
            return redirect('calc', pk=post.pk)
    else:
        form = Form1()
    return render(request, 'registerservice.html', {'form': form})


def detailways(request, pk):
    model1 = get_object_or_404(Model1, pk=pk)
    profile = Profile.objects.filter(user=model1.author)
    # institute = get_object_or_404(Institute,name=model1.institute)
    factor = Factor.objects.all()
    gmaps = googlemaps.Client(key='AIzaSyC87yLIc2D-Qa_ekJktela8KJObYvaZBgg')
    now = datetime.now()
    # print(institute)
    model1.destinationAdress = model1.institute.address
    point1 = model1.institute.startpoint
    try:
        point2 = model1.coordinates.coords
        newpoint2 = []
        newpoint2 = [point2[1], point2[0]]
        newpoint1 = [point1[1], point1[0]]
        print(type(point2))
        # print(point1.coords)
        directions_result = gmaps.directions(newpoint1,
                                             newpoint2,
                                             mode="driving",
                                             avoid="ferries",
                                             departure_time=now, )
        distance0 = directions_result[0]['legs'][0]['distance']['text'].split(' ')[0]
        print(directions_result[0]['legs'][0]['distance']['text'])
        home = gmaps.reverse_geocode(newpoint2)
        home = home[0]['formatted_address']
        dest = gmaps.reverse_geocode(newpoint1)
        dest = dest[0]['formatted_address']
        # print(home[0]['formatted_address'])
        kilometer = directions_result[0]['legs'][0]['distance']['text']
        if float(distance0) < 3:
            price = int(model1.institute.baseprice)
        else:
            price = (float(distance0) // 3) * int(factor[0].factor) + int(model1.institute.baseprice)

        model1.distance = directions_result[0]['legs'][0]['distance']['text']
    except:
        kilometer = 0

    melli = profile[0].user.username
    start = model1.startClassTime
    end = model1.endClassTime
    date = []
    if model1.sat == True:
        date.append('شنبه')
    if model1.sun == True:
        date.append('یکشنبه')
    if model1.mon == True:
        date.append('دوشنبه')
    if model1.tue == True:
        date.append('سه شنبه')
    if model1.wed == True:
        date.append('چهارشنبه')
    if model1.thu == True:
        date.append('پنج شنبه')
    if model1.fri == True:
        date.append('جمعه')

    # model1.destinationAdress = dest
    # model1.sourceAdress = home
    price = model1.price
    name = model1.author.first_name
    lname = model1.author.last_name
    ins = model1.institute.name
    model1.save()
    dest = model1.destinationAdress
    home = model1.sourceAdress
    # return redirect('pay', pk=pk)
    return render(request, 'detailway.html',
                  {'ins': ins, 'name': name, 'lname': lname, 'pk': pk, 'home': home, 'dest': dest,
                   'kilometer': kilometer,
                   'melli': melli, 'end': end,
                   'start': start, 'date': date, 'price': price})


def calc(request, pk):
    model1 = get_object_or_404(Model1, pk=pk)
    profile = Profile.objects.filter(user=model1.author)
    # institute = get_object_or_404(Institute,name=model1.institute)
    factor = Factor.objects.all()
    gmaps = googlemaps.Client(key='AIzaSyC87yLIc2D-Qa_ekJktela8KJObYvaZBgg')
    now = datetime.now()
    # print(institute)
    model1.destinationAdress = model1.institute.address
    point1 = model1.institute.startpoint
    point2 = model1.coordinates.coords
    newpoint2 = []
    newpoint2 = [point2[1], point2[0]]
    newpoint1 = [point1[1], point1[0]]
    print(type(point2))
    # print(point1.coords)
    directions_result = gmaps.directions(newpoint1,
                                         newpoint2,
                                         mode="driving",
                                         avoid="ferries",
                                         departure_time=now, )
    distance0 = directions_result[0]['legs'][0]['distance']['text'].split(' ')[0]
    print(directions_result[0]['legs'][0]['distance']['text'])
    home = gmaps.reverse_geocode(newpoint2)
    home = home[0]['formatted_address']
    dest = gmaps.reverse_geocode(newpoint1)
    dest = dest[0]['formatted_address']
    # print(home[0]['formatted_address'])
    kilometer = directions_result[0]['legs'][0]['distance']['text']
    melli = profile[0].user.username
    start = model1.startClassTime
    end = model1.endClassTime
    date = []
    if model1.sat == True:
        date.append('شنبه')
    if model1.sun == True:
        date.append('یکشنبه')
    if model1.mon == True:
        date.append('دوشنبه')
    if model1.tue == True:
        date.append('سه شنبه')
    if model1.wed == True:
        date.append('چهارشنبه')
    if model1.thu == True:
        date.append('پنج شنبه')
    if model1.fri == True:
        date.append('جمعه')

    if float(distance0) < 3:
        price = int(model1.institute.baseprice)
    else:
        price = (float(distance0) // 3) * int(factor[0].factor) + int(model1.institute.baseprice)

    model1.distance = directions_result[0]['legs'][0]['distance']['text']
    # model1.destinationAdress = dest
    # model1.sourceAdress = home
    model1.price = price
    name = model1.author.first_name
    lname = model1.author.last_name
    ins = model1.institute.name
    model1.save()
    dest = model1.destinationAdress
    home = model1.sourceAdress
    # return redirect('pay', pk=pk)
    return render(request, 'verify.html',
                  {'ins': ins, 'name': name, 'lname': lname, 'pk': pk, 'home': home, 'dest': dest,
                   'kilometer': kilometer,
                   'melli': melli, 'end': end,
                   'start': start, 'date': date, 'price': price})


@login_required()
def index(request):
    student = Profile.objects.filter(role='user1')
    manager = Profile.objects.filter(role='manager')
    managerservice = Profile.objects.filter(role='manager service')
    driver = Profile.objects.filter(role='driver')
    complaint = Complaint.objects.all()
    ins = len(Institute.objects.all())
    term = len(Term.objects.all())
    drv = len(driver)
    man = len(manager) + len(managerservice)
    std = len(student)
    comp = len(complaint)
    if request.user.is_staff:
        return render(request, 'manager.html',
                      {'term': term, 'ins': ins, 'man': man, 'drv': drv, 'std': std, 'comp': comp})
    pk = request.user.pk
    if request.user.profile.role == "manager":
        return render(request, 'managermain.html', {'pk': pk})

    pk = request.user.pk
    if request.user.profile.role == "manager service":
        return render(request, 'managerservicemain.html', {'pk': pk})

    pk = request.user.pk
    if request.user.profile.role == "driver":
        return render(request, 'drivermain.html', {'pk': pk})

    return render(request, 'index.html', {'pk': pk})


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.email = "asd@gmail.com"
            user.refresh_from_db()  # load the profile instance created by the signal
            # user.profile.melli = form.cleaned_data.get('melli')
            user.profile.phone = form.cleaned_data.get('phone')
            user.profile.ephone = form.cleaned_data.get('ephone')
            user.profile.gender = form.cleaned_data.get('gender')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('main')
    else:
        form = SignUpForm()
    return render(request, 'register.html', {'form': form})


@login_required()
def profile(request, pk):
    user1 = get_object_or_404(User, pk=pk)
    profile = get_object_or_404(Profile, pk=pk)
    if request.method == 'POST':
        form_set = SignUpForm2(request.POST, instance=user1)
        form = ProfileForm(request.POST, instance=profile)
        if all([form.is_valid(), form_set.is_valid()]):
            poll = form.save()
            poll.save()
            poll2 = form_set.save()
            poll2.save()
            return redirect('main')

    else:
        form_set = SignUpForm2(instance=user1)
        form = ProfileForm(instance=profile)

    return render(request, 'registeredit.html', {'form': form, 'form_set': form_set})


@login_required()
def factor(request):
    factor, created = Factor.objects.get_or_create(pk=1)
    if request.method == 'POST':
        if created:
            form = FactorForm(request.POST)
        else:
            form = FactorForm(request.POST, instance=factor)
        if form.is_valid():
            poll = form.save()
            poll.save()
            return redirect('main')

    else:
        if created:
            form = FactorForm()
        # form = SignUpForm(instance=user1)
        else:
            form = FactorForm(instance=factor)

    return render(request, 'settings.html', {'form': form})


@login_required()
def history(request, pk):
    user = get_object_or_404(User, pk=pk)
    model1 = Model1.objects.filter(author=user, status=True)
    return render(request, 'history.html', {'history': model1})


@login_required()
def selectdriver(request, pk):
    driver1 = Profile.objects.filter(role="driver")
    profile = get_object_or_404(Profile, pk=pk)
    try:
        model1 = Model1.objects.filter(author=profile.user).filter(status=True).order_by('-model1_date')[0]
        price = model1.price
    except:
        price = 0
    term = Term.objects.filter(status=True)
    c = get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        a = request.POST['my_options']
        b = request.POST['my_options2']
        b = get_object_or_404(Term, name=b)
        a = get_object_or_404(User, username=a)
        DriverStudent.make_student(a, b, c)
        # b.driverstudent.add(drivers)
        print(DriverStudent.objects.all())
        return redirect('studentuserlist')

    return render(request, 'selectdriver.html', {'driver': driver1, 'term': term})


@login_required()
def selectdriverterm(request, pk, slug):
    termdriver = get_object_or_404(Term, name=slug)
    driver1 = termdriver.driver.all()
    profile = get_object_or_404(Profile, pk=pk)
    try:
        model1 = Model1.objects.filter(author=profile.user).filter(status=True).order_by('-model1_date')[0]
        price = model1.price
    except:
        price = 0
    term = Term.objects.filter(status=True)
    c = get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        a = request.POST['my_options']
        # b = request.POST['my_options2']
        b = termdriver
        a = get_object_or_404(User, username=a)
        try:
            model2 = Model1.objects.filter(author=profile.user).filter(term=b)[0]
            DriverStudent.loose_student(model2.driver, b, c)
            print(model2.driver)
        except:
            print('ok')
        model1 = Model1.objects.filter(author=profile.user).filter(term=b)
        for mod in model1:
            mod.driver = a
            mod.save()
        DriverStudent.make_student(a, b, c)
        # b.driverstudent.add(drivers)
        return redirect('studentuserlistterm', slug=slug)

    return render(request, 'selectdriver.html', {'driver': driver1, 'term': term})


def number(request):
    import json
    import requests
    try:
        if request.method == 'POST':
            a = request.POST['numb']
            b = request.POST['name11']
            user = User.objects.get(username=b)
            c = random.randint(1000, 2000)
            c = "parsian@" + str(c)
            print(c)
            user.set_password(c)

            # user.set_password2("@#!$aSde")
            user.save()
            print(profile)
            url = "http://37.130.202.188/services.jspd"
            rcpt_nm = [a]
            param = {'uname': 'chapartaxi', 'pass': '13426', 'from': '+98100020400',
                     'message': 'رمز جدید شما' + c,
                     'to': json.JSONEncoder().encode(rcpt_nm), 'op': 'send'}

            r = requests.post(url, data=param)
            print(r.status_code)
            print(r.text)
            return redirect('login')
    except:
        return render(request, 'number2.html', {})

    return render(request, 'number.html', {})


@login_required()
def complaint(request):
    if request.method == 'POST':

        form = ComplaintForm(request.POST)
        if form.is_valid():
            poll = form.save()
            poll.user = request.user
            poll.save()
            return redirect('main')

    else:

        form = ComplaintForm()

    return render(request, 'complaint.html', {'form': form})


def masssms(request):
    return render(request, 'masssms.html', {})


def masssmsform(request, slug):
    import json
    import requests
    if request.method == 'POST':
        a = request.POST['numb']
        profile = Profile.objects.filter(role=slug)
        b = []
        for pro in profile:
            b.append(profile.phone)

        url = "http://37.130.202.188/services.jspd"
        rcpt_nm = [b]
        param = {'uname': 'chapartaxi', 'pass': '13426', 'from': '+98100020400', 'message': a,
                 'to': json.JSONEncoder().encode(rcpt_nm), 'op': 'send'}

        r = requests.post(url, data=param)
        print(r.status_code)
        print(r.text)
        return render(request, 'masssms.html', {})

    return render(request, 'masssmsform.html', {})


def complaintlist(request):
    comp = Complaint.objects.all()
    return render(request, 'complaintlist.html', {'profile': comp})


def complaintshow(request, pk):
    comp = get_object_or_404(Complaint, pk=pk)
    return render(request, 'complaintshow.html', {'comp': comp})


def complaintdel(request, pk):
    comp = get_object_or_404(Complaint, pk=pk)
    comp.delete()
    return redirect('complaintlist')


def showstudent(request):
    return render(request, 'showstudent.html', {})


def selectreg(request):
    factor = Factor.objects.all()[0]
    disable = factor.disable
    print(disable)
    return render(request, 'selectreg.html', {'disable': disable})


@login_required()
def handregnew(request):
    if request.user.is_staff:
        return render(request, 'manager.html')

    if request.method == "POST":
        form = Formhandy(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.sat = request.POST.get('sat', False)
            post.sun = request.POST.get('sun', False)
            post.mon = request.POST.get('mon', False)
            post.tue = request.POST.get('tue', False)
            post.wed = request.POST.get('wed', False)
            post.thu = request.POST.get('thu', False)
            post.fri = request.POST.get('fri', False)
            print(post.sun)
            post.save()
            return redirect('handycalc', pk=post.pk)
    else:
        form = Formhandy()
    return render(request, 'registerservicehandy.html', {'form': form})


def handycalc(request, pk):
    model1 = get_object_or_404(Model1, pk=pk)
    profile = Profile.objects.filter(user=model1.author)
    # institute = get_object_or_404(Institute,name=model1.institute)
    factor = Factor.objects.all()
    gmaps = googlemaps.Client(key='AIzaSyC87yLIc2D-Qa_ekJktela8KJObYvaZBgg')
    now = datetime.now()
    # print(institute)
    model1.destinationAdress = model1.institute.address
    # point1 = model1.institute.startpoint
    # point2 = model1.coordinates.coords
    # newpoint2 = []
    # newpoint2 = [point2[1], point2[0]]
    # newpoint1 = [point1[1], point1[0]]
    # print(type(point2))
    # # print(point1.coords)
    # directions_result = gmaps.directions(newpoint1,
    #                                      newpoint2,
    #                                      mode="driving",
    #                                      avoid="ferries",
    #                                      departure_time=now, )
    # distance0 = directions_result[0]['legs'][0]['distance']['text'].split(' ')[0]
    # print(directions_result[0]['legs'][0]['distance']['text'])
    # home = gmaps.reverse_geocode(newpoint2)
    # home = home[0]['formatted_address']
    # dest = gmaps.reverse_geocode(newpoint1)
    # dest = dest[0]['formatted_address']
    # # print(home[0]['formatted_address'])

    kilometer = 0
    melli = profile[0].user.username
    start = model1.startClassTime
    end = model1.endClassTime
    date = []
    if model1.sat == True:
        date.append('شنبه')
    if model1.sun == True:
        date.append('یکشنبه')
    if model1.mon == True:
        date.append('دوشنبه')
    if model1.tue == True:
        date.append('سه شنبه')
    if model1.wed == True:
        date.append('چهارشنبه')
    if model1.thu == True:
        date.append('پنج شنبه')
    if model1.fri == True:
        date.append('جمعه')

    # if float(distance0) < 3:
    #     price = int(model1.institute.baseprice)
    # else:
    #     price = (float(distance0) // 3) * int(factor[0].factor) + int(model1.institute.baseprice)
    #
    model1.distance = 0

    # model1.destinationAdress = dest
    # model1.sourceAdress = home
    price = model1.handyprice
    model1.price = model1.handyprice
    name = model1.author.first_name
    lname = model1.author.last_name
    ins = model1.institute.name
    model1.save()
    dest = model1.destinationAdress
    home = model1.sourceAdress
    # return redirect('pay', pk=pk)
    return render(request, 'verify.html',
                  {'ins': ins, 'name': name, 'lname': lname, 'pk': pk, 'home': home, 'dest': dest,
                   'kilometer': kilometer,
                   'melli': melli, 'end': end,
                   'start': start, 'date': date, 'price': price})


@login_required()
def studentwithoutdriver(request, slug):
    model1 = Model1.objects.filter(term__name=slug).filter(status=True)
    term = Term.objects.filter(name=slug)
    student = []
    institute = []
    driver = []
    print(model1)
    modelpk = []
    for usr in model1:
        if (usr.driver == None):
            print(usr.driver)
            modelpk.append(usr.pk)
            helper = get_object_or_404(Profile, user=usr.author)
            institute.append(usr)
            driver.append(usr.driver)
            student.append(helper)
    student = student[::-1]
    institute = institute[::-1]
    driver = driver[::-1]
    modelpk = modelpk[::-1]
    student = list(zip(student, institute, driver, modelpk))
    return render(request, 'studentwithoutdriver.html', {'student': student, 'slug': slug})


@login_required()
def canceldesc(request, pk, slug):
    model = get_object_or_404(Model1, pk=pk)
    if request.method == 'POST':

        form = CancelDescForm(request.POST, instance=model)
        if form.is_valid():
            poll = form.save()
            poll.cancel = True
            poll.save()
            return redirect('studentwithoutdriver', slug=slug)

    else:

        form = CancelDescForm()
    return render(request, 'canceldesc.html', {'form': form})


def disablehandypay(request):
    fact = Factor.objects.all()[0]
    fact.disable = False
    print(fact.disable)
    fact.save()
    return redirect('factor')


def enablehandypay(request):
    fact = Factor.objects.all()[0]
    fact.disable = True
    print(fact.disable)
    fact.save()
    return redirect('factor')
