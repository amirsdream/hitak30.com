from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views
from .forms import LoginForm

#
urlpatterns = [
    url(r'^$', views.index, name='main'),
    url(r'^regnew/$', views.regnew, name='reg'),
    url(r'^enablehandypay/$', views.enablehandypay, name='enablehandypay'),
    url(r'^disablehandypay/$', views.disablehandypay, name='disablehandypay'),
    url(r'^handregnew/$', views.handregnew, name='handreg'),
    url(r'^number/$', views.number, name='number'),
    url(r'^masssms/$', views.masssms, name='masssms'),
    url(r'^masssmsform/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.masssmsform, name='masssmsform'),
    url(r'^studentwithoutdriver/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.studentwithoutdriver,
        name='studentwithoutdriver'),
    url(r'^complaint/$', views.complaint, name='complaint'),
    url(r'^canceldesc/(?P<pk>\d+)/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.canceldesc, name='canceldesc'),
    url(r'^profile/(?P<pk>\d+)/$', views.profile, name='profile'),
    url(r'^calc/(?P<pk>\d+)/$', views.calc, name='calc'),
    url(r'^handycalc/(?P<pk>\d+)/$', views.handycalc, name='handycalc'),
    url(r'^detailways/(?P<pk>\d+)/$', views.detailways, name='detailways'),
    url(r'^selectdriver/(?P<pk>\d+)/$', views.selectdriver, name='selectdriver'),
    url(r'^selectdriverterm/(?P<pk>\d+)/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.selectdriverterm,
        name='selectdriverterm'),
    url(r'^history/(?P<pk>\d+)/$', views.history, name='history'),
    url(r'^factor/$', views.factor, name='factor'),
    url(r'^selectreg/$', views.selectreg, name='selectreg'),
    url(r'^complaintlist/$', views.complaintlist, name='complaintlist'),
    url(r'^complaintshow/(?P<pk>\d+)/$', views.complaintshow, name='complaintshow'),
    url(r'^complaintdel/(?P<pk>\d+)/$', views.complaintdel, name='complaintdel'),
    url(r'^showstudent/$', views.showstudent, name='showstudent'),
    url(r'^register/$', views.signup, name='register'),
    # url(r'^login$', views.login, name='login'),
    url(r'^login/$', auth_views.login, {'template_name': 'login.html', 'authentication_form': LoginForm},
        name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'login'}, name='logout'),

]
