from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from stdimage.models import StdImageField
from datetime import datetime
from institute.models import Institute, Term
from django.core.validators import MaxValueValidator
from django.utils import timezone

# POINT = Point(-104.9903, 39.7392, srid=4326)

classDate1 = (
    ('shanbe', u'شنبه'),
    ('yekshanbe', u'یکشنبه'),
    ('doshanbe', u'دوشنبه'),
    ('seshanbe', u'سه شنبه'),
    ('chaharshanbe', u'چهارشنبه'),
    ('panjshanbe', u'پنج شنبه '),
    ('jome', u'جمعه'),
)

roleType = (
    ('user1', u'کاربر'),
    ('manager', u'مدیراجرایی'),
    ('manager service', u'مدیرسرویس'),
    ('driver', u'راننده')
)

genderType = (
    ('male', u'آقا'),
    ('female', u'خانم'),
)
stateType = (
    ('True', u'فعال'),
    ('False', u'غیر فعال'),
    ('Cancel', u'انصراف')
)
bankType = (
    ('mellat', 'ملت'),
    ('tejarat', u'تجارت'),
)


# Create your models here.
class Model1(models.Model):
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, blank=True, null=True)
    term = models.ForeignKey(Term, on_delete=models.CASCADE, blank=True, null=True)
    driver = models.ForeignKey(User, related_name='drivermodel', on_delete=models.CASCADE, blank=True, null=True)
    manager = models.ForeignKey(User, related_name='managermodel', on_delete=models.CASCADE, blank=True, null=True)
    servicemanager = models.ForeignKey(User, related_name='servicemanagermodel', on_delete=models.CASCADE, blank=True,
                                       null=True)
    model1_date = models.DateTimeField(default=datetime.now)
    author = models.ForeignKey('auth.User', default=1)
    destinationAdress = models.TextField()
    coordinates = models.PointField(null=True, help_text="To generate the map for your location")
    city_hall = models.PointField(null=True, blank=True)
    sourceAdress = models.TextField()
    classDate = models.ManyToManyField('DateUse')
    startClassTime = models.TimeField()
    endClassTime = models.TimeField()
    price = models.DecimalField(max_digits=10, decimal_places=0, default=1350000)
    handyprice = models.DecimalField(max_digits=10, decimal_places=0, default=0)
    status = models.BooleanField(default=False)
    sat = models.BooleanField(default=False)
    sun = models.BooleanField(default=False)
    mon = models.BooleanField(default=False)
    tue = models.BooleanField(default=False)
    wed = models.BooleanField(default=False)
    thu = models.BooleanField(default=False)
    fri = models.BooleanField(default=False)
    distance = models.CharField(max_length=30, null=True, blank=True)
    desc = models.TextField(null=True, blank=True)
    cancel = models.BooleanField(default=False)


class DateUse(models.Model):
    description = models.CharField(max_length=300, choices=classDate1, default='shanbe')


class Model2(models.Model):
    price = models.DecimalField(max_digits=10, decimal_places=0)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.DecimalField(max_digits=11, decimal_places=0, null=True, blank=True)
    ephone = models.DecimalField(max_digits=11, decimal_places=0, null=True, blank=True)
    melli = models.DecimalField(max_digits=11, decimal_places=0, null=True, blank=True)
    role = models.CharField(max_length=100, choices=roleType, default='user1', null=True, blank=True)
    gender = models.CharField(max_length=100, choices=genderType, default='male', null=True, blank=True)
    image2 = StdImageField(upload_to='profileimage', blank=True, null=True,
                           variations={'thumbnail': (400, 400, True), 'thumbnail2': (200, 200, True), })

    driver = models.CharField(max_length=100, null=True, blank=True)
    fathername = models.CharField(max_length=100, null=True, blank=True)
    bankacc = models.CharField(max_length=100, null=True, blank=True)
    # term = models.CharField(max_length=100, null=True ,blank=True)
    bankaccnum = models.PositiveIntegerField(validators=[MaxValueValidator(9999999999)], default=0000000000, null=True,
                                             blank=True)
    bankaccname = models.CharField(max_length=100, default="mellat", choices=bankType, null=True, blank=True)
    postalcode = models.CharField(max_length=100, null=True, blank=True)
    marriagestat = models.CharField(max_length=100, null=True, blank=True)
    education = models.CharField(max_length=100, null=True, blank=True)
    validation = models.CharField(max_length=100, null=True, blank=True)
    insurance = models.CharField(max_length=100, null=True, blank=True)
    insurancenum = models.CharField(max_length=100, null=True, blank=True)
    license = models.CharField(max_length=100, null=True, blank=True)
    licensedate = models.CharField(max_length=100, null=True, blank=True)
    licensenum = models.CharField(max_length=100, null=True, blank=True)
    licenseduration = models.CharField(max_length=100, null=True, blank=True)
    auttype = models.CharField(max_length=100, null=True, blank=True)
    carnum = models.CharField(max_length=100, null=True, blank=True)
    autmodel = models.CharField(max_length=100, null=True, blank=True)
    techcheck = models.CharField(max_length=100, null=True, blank=True)
    motornum = models.CharField(max_length=100, null=True, blank=True)
    autcolor = models.CharField(max_length=100, null=True, blank=True)
    reltype = models.CharField(max_length=100, null=True, blank=True)
    vasighe = models.CharField(max_length=100, null=True, blank=True)
    state = models.CharField(max_length=100, choices=stateType, null=True, blank=True, default='True')

    # image2 = StdImageField(upload_to='profileimage', blank=True, null=True,
    #                        variations={'thumbnail': (400, 400, True), 'thumbnail2': (200, 200, True), })
    canceldate = models.CharField(max_length=100, null=True, blank=True)
    cancelcause = models.CharField(max_length=100, null=True, blank=True)
    joindate = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    description = models.CharField(max_length=100, null=True, blank=True)
    disable = models.BooleanField(default=False)
    salary1 = models.ForeignKey('Salary', on_delete=models.CASCADE, blank=True, null=True)
    # salary1 = models.ForeignKey(salary,on_delete=models.CASCADE, blank=True, null=True ,blank=True)
    # role = models.CharField(max_length=100, choices=roleType, default='driver')


class Driver(models.Model):
    diver = models.CharField(max_length=100, null=True, blank=True)


# class Student(models.Model):
#     driver = models.ForeignKey('Driver', default=1)


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class City(models.Model):
    name = models.CharField(max_length=255)
    coordinates = models.PointField(help_text="To generate the map for your location")
    city_hall = models.PointField(blank=True, null=True)

    def __unicode__(self):
        return self.name


class District(models.Model):
    city = models.ForeignKey(City)
    name = models.CharField(max_length=255)
    location = models.PointField(help_text="To generate the map for your location")

    def __unicode__(self):
        return self.name


class Factor(models.Model):
    factor = models.CharField(max_length=100, default=1)
    factor2 = models.CharField(max_length=100, default=1)
    factor3 = models.CharField(max_length=100, default=1)
    percent1 = models.CharField(max_length=100, default=1)
    percent2 = models.CharField(max_length=100, default=1)
    percent3 = models.CharField(max_length=100, default=1)
    disable = models.BooleanField(default=False)


class Complaint(models.Model):
    author = models.ForeignKey('auth.User', default=1)
    drivername = models.CharField(max_length=100, null=True, blank=True)
    reason = models.CharField(max_length=100, null=True, blank=True)
    desc = models.TextField(null=True, blank=True)


class Salary(models.Model):
    # mainuser = models.ForeignKey(User, on_delete=models.CASCADE)
    driversalary = models.DecimalField(max_digits=11, decimal_places=0, null=True, blank=True)
    presalary = models.DecimalField(max_digits=11, decimal_places=0, null=True, blank=True)
    managersalary = models.DecimalField(max_digits=11, decimal_places=0, null=True, blank=True)
    termsalary = models.ForeignKey(Term, on_delete=models.CASCADE, blank=True, null=True)
