from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User  # fill in custom user info then save it
from mapwidgets.widgets import GooglePointFieldWidget

from .models import Model1, Model2, Profile, Factor, Complaint


class MyRegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'password1', 'password2')
        widgets = {
            'username': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated'}),
            'password1': forms.TextInput(attrs={'type': 'password', 'class': 'form-control parsley-validated'}),
            'password2': forms.TextInput(attrs={'type': 'password', 'class': 'form-control parsley-validated'}),
        }
        labels = {
            'username': u'کد ملی',
            'email': u'ایمیل',
            'password1': u'رمز',
            'password2': u'تکرار رمز',
            'first_name': u'نام',
        }

    def save(self, commit=True):
        user = super(MyRegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_ame']
        user.last_name = self.cleaned_data['last_name']
        user.birthday = self.cleaned_data['birthday']

        if commit:
            user.save()

        return user


class Form1(forms.ModelForm):
    class Meta:
        model = Model1
        fields = ('startClassTime', 'endClassTime', 'institute', 'term', 'coordinates', 'sourceAdress')
        help_texts = {'startClassTime': u"ساعت را به صورت  12:00 وارد کنید.", }
        widgets = {'coordinates': GooglePointFieldWidget, 'city_hall': GooglePointFieldWidget,
                   'startClassTime': forms.TextInput(
                       attrs={'type': 'text', 'class': 'time24', 'id': 'time24', 'placeholder': 'مثال 12:00'}),
                   'endClassTime': forms.TextInput(attrs={'type': 'text', 'placeholder': 'مثال 12:00'}),
                   'institute': forms.Select(attrs={})}
        # widgets = {
        #     'sourceAdress': forms.Textarea(attrs={'type': 'text', 'class': 'form-control parsley-validated'}),
        #     'destinationAdress': forms.Textarea(attrs={'type': 'text', 'class': 'form-control parsley-validated'}),
        #     'startClassTime': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated'}),
        #     'endClassTime': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated'}),
        #     'price': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated'}),
        #
        # }
        labels = {
            'sourceAdress': u'آدرس منزل',
            'destinationAdress': u'آدرس کانون',
            'startClassTime': u'ساعت شروع کلاس',
            'endClassTime': u'ساعت پایان کلاس',
            'classDate': u'ساعات کلاس',
            'coordinates': u'منزل',
            'city_hall': 'آدرس کانون',
            'term': u'ترم',
            'institute': u'کانون',
        }

class Formhandy(forms.ModelForm):
    class Meta:
        model = Model1
        fields = ('startClassTime', 'endClassTime', 'institute', 'term', 'handyprice', 'sourceAdress')
        help_texts = {'startClassTime': u"ساعت را به صورت  12:00 وارد کنید.", }
        widgets = {'coordinates': GooglePointFieldWidget, 'city_hall': GooglePointFieldWidget,
                   'startClassTime': forms.TextInput(
                       attrs={'type': 'text', 'class': 'time24', 'id': 'time24', 'placeholder': 'مثال 12:00'}),
                   'endClassTime': forms.TextInput(attrs={'type': 'text', 'placeholder': 'مثال 12:00'}),
                   'institute': forms.Select(attrs={})}
        # widgets = {
        #     'sourceAdress': forms.Textarea(attrs={'type': 'text', 'class': 'form-control parsley-validated'}),
        #     'destinationAdress': forms.Textarea(attrs={'type': 'text', 'class': 'form-control parsley-validated'}),
        #     'startClassTime': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated'}),
        #     'endClassTime': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated'}),
        #     'price': forms.TextInput(attrs={'type': 'text', 'class': 'form-control parsley-validated'}),
        #
        # }
        labels = {
            'sourceAdress': u'آدرس منزل',
            'destinationAdress': u'آدرس کانون',
            'startClassTime': u'ساعت شروع کلاس',
            'endClassTime': u'ساعت پایان کلاس',
            'classDate': u'ساعات کلاس',
            'coordinates': u'منزل',
            'city_hall': 'آدرس کانون',
            'term': u'ترم',
            'institute': u'کانون',
            'handyprice':u'قیمت',
        }



# class Form2(forms.ModelForm):
class Form2(forms.ModelForm):
    class Meta:
        model = Model2
        fields = ('price',)


class SignUpForm(UserCreationForm):
    genderType = (
        ('male', u'آقا'),
        ('female', u'خانم'),
    )
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    # melli = forms.DecimalField()
    phone = forms.DecimalField()
    ephone = forms.DecimalField()
    gender = forms.ChoiceField(choices=genderType)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'password1', 'password2', 'phone', 'ephone',)

        labels = {
            'username': u'کدملی',
            'email': u'ایمیل',
            'password1': u'رمز',
            'password2': u'تکرار رمز',
            'First name': u'نام',
            'last_name': u'نام خانوادگی',
            'melli': u'کد ملی',
            'phone': u'شماره تماس',
            'ephone': u'شماره تماس اضطراری',
            'gender': u'جنسیت'
        }

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        # self.fields['email'].label = "ایمیل"
        self.fields['first_name'].label = "نام"
        self.fields['last_name'].label = "نام خانوادگی"
        self.fields['password1'].label = "رمز"
        self.fields['password2'].label = "تکرار رمز"
        # self.fields['melli'].label = "کد ملی"
        self.fields['phone'].label = "تلفن"
        self.fields['ephone'].label = "تلفن ضروری"
        self.fields['gender'].label = "جنسیت"

    class ProfileForm(forms.ModelForm):
        class Meta:
            model = Profile
            fields = (
                 'phone', 'ephone', 'gender')
            labels = {

                'phone': u'شماره تماس',
                'ephone': u'شماره تماس اضطراری',
                'gender': 'جنسیت',
            }

class SignUpForm2(UserCreationForm):
    genderType = (
        ('male', u'آقا'),
        ('female', u'خانم'),
    )
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    # melli = forms.DecimalField()
    # phone = forms.DecimalField()
    # ephone = forms.DecimalField()
    # gender = forms.ChoiceField(choices=genderType)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'password1', 'password2', )

        labels = {
            'username': u'کدملی',
            'email': u'ایمیل',
            'password1': u'رمز',
            'password2': u'تکرار رمز',
            'First name': u'نام',
            'last_name': u'نام خانوادگی',
            'melli': u'کد ملی',

        }

    def __init__(self, *args, **kwargs):
        super(SignUpForm2, self).__init__(*args, **kwargs)
        # self.fields['email'].label = "ایمیل"
        self.fields['first_name'].label = "نام"
        self.fields['last_name'].label = "نام خانوادگی"
        self.fields['password1'].label = "رمز"
        self.fields['password2'].label = "تکرار رمز"
        # self.fields['melli'].label = "کد ملی"
        # self.fields['phone'].label = "تلفن"
        # self.fields['ephone'].label = "تلفن ضروری"
        # self.fields['gender'].label = "جنسیت"




class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30, widget=forms.TextInput(
        attrs={'type': 'text', 'class': 'form-control input-lg', 'name': 'username', 'id': 'username'}))
    password = forms.CharField(label="Password", max_length=30, widget=forms.TextInput(
        attrs={'type': 'password', 'class': 'form-control input-lg', 'name': 'password', 'id': 'password'}))


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = (
             'phone', 'ephone', 'gender')
        labels = {

            'phone': u'شماره تماس',
            'ephone': u'شماره تماس اضطراری',
            'gender': 'جنسیت'
        }


class FactorForm(forms.ModelForm):
    class Meta:
        model = Factor
        fields = ('factor','factor2','factor3', 'percent1', 'percent2','percent3',)
        labels = {
            'factor': u'هزینه هر ۳ کیلومتر',
            'factor2': u'درصد کانون از درآمد',
            'factor3': u'درصد مدیر اجرایی از درآمد',
            'percent1': u'درصد راننده از درآمد',
            'percent2': u'درصد مدیر سرویس از درآمد',
            'percent3': u'درصد شرکت',

        }


class DriveForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('driver',)


class ComplaintForm(forms.ModelForm):
    class Meta:
        model = Complaint
        exclude = ('author',)

        labels = {
            'drivername': u'تام راننده',
            'reason': u'عنوان شکایت',
            'desc': u'توضیحات',

        }

class CancelDescForm(forms.ModelForm):
    class Meta:
        model = Model1
        fields = ('desc',)

        labels = {
            'desc': u'توضیح',
        }
