# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-06-29 12:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registeration', '0066_auto_20180626_1613'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='joindate',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
