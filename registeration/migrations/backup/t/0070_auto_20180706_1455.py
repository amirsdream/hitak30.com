# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-07-06 10:25
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registeration', '0069_auto_20180706_1453'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='city',
            name='city_hall',
        ),
        migrations.RemoveField(
            model_name='city',
            name='coordinates',
        ),
    ]
