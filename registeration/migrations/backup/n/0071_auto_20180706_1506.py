# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-07-06 10:36
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registeration', '0070_city_district'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='city_hall',
            field=django.contrib.gis.db.models.fields.PointField(null=True, srid=4326),
        ),
        migrations.AlterField(
            model_name='city',
            name='coordinates',
            field=django.contrib.gis.db.models.fields.PointField(help_text='To generate the map for your location', null=True, srid=4326),
        ),
    ]
