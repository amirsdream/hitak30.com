# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-04-07 13:05
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registeration', '0026_driverprofile_image2'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='driverprofile',
            name='user',
        ),
        migrations.RemoveField(
            model_name='model1',
            name='driver',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='role',
        ),
        migrations.DeleteModel(
            name='DriverProfile',
        ),
    ]
