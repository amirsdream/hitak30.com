from django.shortcuts import render
import django_excel as excel
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404
from driver.models import Accounting, TotalSalary
from institute.models import Term, DriverStudent, Institute
from registeration.models import Profile, Model1, Factor
from manager.models import Manager, ManagerService
from django.db.models import Q
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect
from .models import Gpayments, Calcall, PaySelected, TransferFundCancel
from institute.forms import TsalaryConfirmForm


# Create your views here.

@login_required()
def calclist(request, slug):
    term = Term.objects.filter(name=slug)
    a = Calcall
    a.calcsalary(slug=slug)
    gpayments = Gpayments.objects.filter(term=term[0])
    stddriverar = []
    pricear = []
    statusar = []
    for i in gpayments:
        if i.profile:
            stddriverar.append(i.profile)
        else:
            name = Institute.objects.filter(name=i.name)
            stddriverar.append(name[0])
        pricear.append(i.price)
        statusar.append(i.status)
    profile = zip(stddriverar, pricear, statusar)
    if request.method == 'POST':
        if 'message_frm' in request.POST:
            a = request.POST['my_options1']
            b = request.POST['my_options2']
            c = request.POST['my_options3']
            slug2 = a + '?' + b + '?' + c
            return redirect('filter', slug=slug, slug2=slug2)
        if 'message_frm2' in request.POST:
            selected_values = request.POST.getlist('sclid')
            x = PaySelected.pay(term=term[0], selectedvalues=selected_values)
            filename = "ELFL.txt"
            content = x
            response = HttpResponse(content, content_type='text/plain')
            response['Content-Disposition'] = 'attachment; filename={0}'.format(filename)
            return response
            # return redirect('calclist', slug=slug)
    return render(request, 'grouppayment.html', {'profile': profile, 'slug': slug})


def calclistajax(requests):
    object_list = TotalSalary.objects.all()  # or any kind of queryset
    json = serializers.serialize('json', object_list)
    print(json)
    return HttpResponse(json, content_type='application/json')


@login_required()
def filter(request, slug, slug2):
    a = Calcall
    a.calcsalary(slug=slug)
    term = Term.objects.filter(name=slug)
    gpayments = Gpayments.objects.filter(term=term[0])
    stddriverar = []
    pricear = []
    statusar = []
    filt = slug2.split("?")

    for i in gpayments:
        if i.profile:
            if i.profile.bankaccname == filt[0]:
                if i.profile.state == filt[1]:
                    if i.profile.role == filt[2]:
                        stddriverar.append(i.profile)
                        pricear.append(i.price)
                        statusar.append(i.status)
                    elif 'all' == filt[2]:
                        stddriverar.append(i.profile)
                        pricear.append(i.price)
                        statusar.append(i.status)
                elif 'all' == filt[1]:
                    print(i.profile.role == filt[2])
                    if i.profile.role == filt[2]:
                        stddriverar.append(i.profile)
                        pricear.append(i.price)
                        statusar.append(i.status)
                    elif 'all' == filt[2]:
                        stddriverar.append(i.profile)
                        pricear.append(i.price)
                        statusar.append(i.status)
        else:
            name = Institute.objects.filter(name=i.name)
            if name[0].bankaccname == filt[0]:
                if str(name[0].status) == filt[1]:
                    if 'institute' == filt[2]:
                        stddriverar.append(name[0])
                        pricear.append(i.price)
                        statusar.append(i.status)
                    elif 'all' == filt[2]:
                        stddriverar.append(name[0])
                        pricear.append(i.price)
                        statusar.append(i.status)
                elif 'all' == filt[1]:
                    if 'institute' == filt[2]:
                        stddriverar.append(name[0])
                        pricear.append(i.price)
                        statusar.append(i.status)
                    elif 'all' == filt[2]:
                        stddriverar.append(name[0])
                        pricear.append(i.price)
                        statusar.append(i.status)

    profile = zip(stddriverar, pricear, statusar)
    if request.method == 'POST':
        if 'message_frm' in request.POST:
            a = request.POST['my_options1']
            b = request.POST['my_options2']
            c = request.POST['my_options3']
            slug2 = a + '?' + b + '?' + c
            return redirect('filter', slug=slug, slug2=slug2)
    if 'message_frm2' in request.POST:
        selected_values = request.POST.getlist('sclid')
        x = PaySelected.pay(term=term[0], selectedvalues=selected_values)
        filename = "ELFL.txt"
        content = x
        response = HttpResponse(content, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename={0}'.format(filename)
        return response
        # return redirect('calclist', slug=slug)
    return render(request, 'grouppayment.html', {'profile': profile, 'slug': slug})


@login_required()
def transferfundcancelone(request, slug, slug2, pk):
    term = Term.objects.filter(name=slug)
    if slug2 == 'institute':
        profile = get_object_or_404(Institute, pk=pk)
    else:
        profile = get_object_or_404(Profile, pk=pk)
    TransferFundCancel.transferfundcancel(term=term[0], value=profile, role=slug2)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required()
def payconfirmone(request, slug, pk, slug2):
    term = Term.objects.filter(name=slug)
    if request.method == 'POST':
        if request.POST:
            a = request.POST['my_options1']
            if slug2 == 'institute':
                ins = get_object_or_404(Institute,pk=pk)
                tsalary = TotalSalary.objects.filter(institute=ins)
                for t in tsalary:
                    t.receipt = a
                    t.status = "True"
                    t.save()
                return redirect('calclist', slug=slug)

            else:
                user = get_object_or_404(User, pk=pk)
                tsalary = TotalSalary.objects.filter(user=user)
                for t in tsalary:
                    t.receipt = a
                    t.status = "True"
                    t.save()
                return redirect('calclist', slug=slug)

    return render(request, 'paymentconfirm.html', {})
