from django.db import models
# from django.shortcuts import get_object_or_404
from registeration.models import Profile
from institute.models import Term
from driver.models import Accounting, TotalSalary
from institute.models import Term, DriverStudent, Institute
from registeration.models import Profile, Model1, Factor
from manager.models import Manager, ManagerService, TermIns
from django.shortcuts import get_object_or_404, HttpResponse


# Create your models here.
class Gpayments(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, blank=True, null=True)
    term = models.ForeignKey(Term, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(null=True, max_length=30)
    price = models.CharField(null=True, max_length=30)
    status = models.CharField(null=True, max_length=30)
    role = models.CharField(null=True, max_length=30)

    def __unicode__(self):
        return "%s" % self.name


class Calcall(models.Model):
    @staticmethod
    def calcsalary(slug):
        term = Term.objects.filter(name=slug)
        factor = Factor.objects.all()
        profile = term[0].driver.all()
        price = 0
        stddriverar = []
        pricear = []
        statusar = []
        insdrv = []
        for pro in profile:
            try:
                factor1 = factor[0].percent1
                prof = get_object_or_404(Profile, pk=pro.pk)
                stddriverar.append(prof)
                price = Calcone.calculator(term=term, pro=pro, factor=factor1)
                tsalary, created = TotalSalary.objects.get_or_create(user=pro, term=term[0], role='driver')
                tsalary.basesalary = price
                accountdriver = Accounting.objects.filter(author=pro, term=term[0], role='driver')

                for accdrv in accountdriver:
                    if accdrv.account == "minus":
                        price = float(price) - float(accdrv.subprice)

                    else:
                        price = float(price) + float(accdrv.subprice)

                if tsalary.status == "False":
                    statusar.append("پرداخت نشده")
                    tsalary.user = pro
                    tsalary.term = term[0]
                    tsalary.totalsalary = price
                    tsalary.save()
                elif tsalary.status == 'progress':
                    statusar.append("بررسی پرداخت")
                    price = tsalary.totalsalary
                else:
                    statusar.append("پرداخت شده")
                    price = tsalary.totalsalary
                pricear.append(price)
            except Exception as e:
                s = str(e)
                print(s)
        # profile = list(zip(stddriverar, pricear, statusar))
        term = Term.objects.filter(name=slug)
        ins = Institute.objects.all()
        factor = Factor.objects.all()
        for pro in ins:
            price = 0
            stddriverar.append(pro)

            tsalary, created = TotalSalary.objects.get_or_create(institute=pro)
            model1 = Model1.objects.filter(institute=pro).filter(term=term[0]).filter(status=True)
            for helper in model1:
                price += helper.price
            price = price * int(factor[0].factor2) / 100
            if tsalary.status == 'False':
                statusar.append("پرداخت نشده")
                tsalary.institute = pro
                tsalary.totalsalary = price
                tsalary.term = term[0]
                tsalary.save()
            elif tsalary.status == 'progress':
                statusar.append("بررسی پرداخت")
                price = tsalary.totalsalary

            else:
                statusar.append("پرداخت شده")
                price = tsalary.totalsalary
            pricear.append(price)
        term = Term.objects.filter(name=slug)
        factor = Factor.objects.all()
        price = 0
        try:
            profile = term[0].manager.all()
            for pro2 in profile:
                managermanager = get_object_or_404(Manager, manager=pro2)
                termins = TermIns.objects.filter(user=managermanager.manager, term=term[0])
                termins = termins[0].institute.all()
                print(termins)
                manserv = managermanager.managerservices.all()
                prof = get_object_or_404(Profile, pk=pro2.pk)
                stddriverar.append(prof)
                factor1 = factor[0].factor3
                price = 0
                for pro1 in manserv:
                    managerdrv = get_object_or_404(ManagerService, managerservice=pro1)
                    mandrv = managerdrv.drivers.all()

                    for pro in mandrv:
                        for i in termins:
                            try:
                                price += Calctwo.calculator(term=term, pro=pro, factor=factor1, institute=i.name)

                            except Exception as e:
                                s = str(e)
                                print(s)
                tsalary, created = TotalSalary.objects.get_or_create(user=pro2, role='manager', term=term[0])
                tsalary.basesalary = price
                accountdriver = Accounting.objects.filter(author=pro2, term=term[0], role='manager')

                for accdrv in accountdriver:
                    if accdrv.account == "minus":
                        price = float(price) - float(accdrv.subprice)
                    else:
                        price = float(price) + float(accdrv.subprice)

                if tsalary.status == 'False':
                    statusar.append("پرداخت نشده")
                    tsalary.user = pro2
                    tsalary.term = term[0]
                    tsalary.totalsalary = price
                    tsalary.save()
                elif tsalary.status == 'progress':
                    statusar.append("بررسی پرداخت")
                    price = tsalary.totalsalary
                else:
                    statusar.append("پرداخت شده")
                    price = tsalary.totalsalary
                pricear.append(price)

        except Exception as e:
            s = str(e)
            print(s)
        term = Term.objects.filter(name=slug)
        factor = Factor.objects.all()

        price = 0
        try:
            profile = term[0].managerservice.all()

            for pro1 in profile:

                managerdrv = get_object_or_404(ManagerService, managerservice=pro1, term=term[0])
                termins = TermIns.objects.filter(user=managerdrv.managerservice, term=term[0])
                termins = termins[0].institute.all()
                print(termins)
                mandrv = managerdrv.drivers.all()
                prof = get_object_or_404(Profile, pk=pro1.pk)
                stddriverar.append(prof)

                price = 0
                for pro in mandrv:
                    for i in termins:
                        try:
                            factor1 = factor[0].percent2
                            prof = get_object_or_404(Profile, pk=pro1.pk)
                            stddriverar.append(prof)
                            price += Calctwo.calculator(term=term, pro=pro, factor=factor1, institute=i.name)

                        except Exception as e:
                            s = str(e)
                            print(s)

                tsalary, created = TotalSalary.objects.get_or_create(user=pro1, term=term[0], role='manager service')

                tsalary.basesalary = price

                accountdriver = Accounting.objects.filter(author=pro1, term=term[0], role='manager service')
                # print(managerdrv.managerservice)
                for accdrv in accountdriver:
                    if accdrv.account == "minus":
                        # price-=float(accdrv.subprice)
                        price = float(price) - float(accdrv.subprice)
                        # pass
                    else:
                        price = float(price) + float(accdrv.subprice)
                if tsalary.status == 'False':
                    statusar.append("پرداخت نشده")
                    tsalary.user = pro1
                    tsalary.term = term[0]
                    tsalary.totalsalary = price
                    tsalary.save()
                elif tsalary.status == 'progress':
                    statusar.append("بررسی پرداخت")
                    price = tsalary.totalsalary
                else:
                    statusar.append("پرداخت شده")
                    price = tsalary.totalsalary
                pricear.append(price)
        except Exception as e:
            s = str(e)
            print(s)
        h = zip(stddriverar, pricear, statusar)
        profile = list(h)
        term = Term.objects.filter(name=slug)
        gpayment = Gpayments.objects.all()
        for i in gpayment:
            gpayment.delete()
        for i in range(len(pricear)):

            try:
                gpayment, created = Gpayments.objects.get_or_create(profile=stddriverar[i], term=term[0],
                                                                    role=stddriverar[i].role)
            except:
                gpayment, created = Gpayments.objects.get_or_create(name=str(stddriverar[i]), term=term[0],
                                                                    role='institute')
            t = 0
            if gpayment.price:
                t = float(pricear[i]) + float(gpayment.price)
            else:
                t = pricear[i]
            if t:
                gpayment.price = t
                gpayment.status = statusar[i]
                gpayment.save()
            else:
                gpayment.delete()


class Calcone:
    @staticmethod
    def calculator(term, factor, pro):
        stddriver = DriverStudent.objects.filter(driver=pro).filter(term=term)[0]
        helper = stddriver.students.all()
        price = 0
        for stddrv in helper:
            helper2 = Model1.objects.filter(author=stddrv, term=term[0], status=True)
            for help in helper2:
                price += help.price

        price = price * int(factor) / 100

        return price


class Calctwo:
    @staticmethod
    def calculator(term, factor, pro, institute):
        stddriver = DriverStudent.objects.filter(driver=pro).filter(term=term)[0]
        helper = stddriver.students.all()
        price = 0
        institute = get_object_or_404(Institute, name=institute)
        for stddrv in helper:
            helper2 = Model1.objects.filter(author=stddrv, term=term[0], status=True, institute=institute)
            for help in helper2:
                price += help.price

        price = price * int(factor) / 100

        return price


class PaySelected:
    @staticmethod
    def pay(term, selectedvalues):
        p = []
        for i in selectedvalues:
            j = i.split(',', 1)
            print(j)
            if j[1] == "institute":
                p.append(get_object_or_404(Institute, pk=j[0]))
            else:
                p.append(get_object_or_404(Profile, pk=j[0]))
        return TransferFund.transferfund(term, p)


class TransferFund:
    @staticmethod
    def transferfund(term, profiles):
        x = []
        for p in profiles:
            try:
                s = p.user
                print(p.user)
                a = '0000000000'
                b = '00000000000000000'
                try:
                    a = p.bankaccnum
                    tsalary = TotalSalary.objects.filter(user=p.user).filter(term=term)
                    ttsalary = 0
                    help = 0
                    bhelp = 0
                    for i in tsalary:
                        if i.status != "True":
                            ttsalary += i.totalsalary
                    if ttsalary:
                        help = ttsalary + 1000000000000000
                        bhelp = int(a) + 10000000000
                        x.append(str(bhelp)[1:11] + (str(help))[1:16] + b + '                             ' + '\n')
                        for i in tsalary:
                            i.status = "progress"
                            i.save()
                    # print(a)
                except Exception as e:
                    s = str(e)
                    print(s)
            except:
                print(p.name)
                a = '0000000000'
                b = '00000000000000000'
                try:
                    a = p.bankaccnum
                    tsalary = TotalSalary.objects.filter(institute=p).filter(term=term)
                    ttsalary = 0
                    help = 0
                    bhelp = 0
                    for i in tsalary:
                        if i.status != "True":
                            ttsalary += i.totalsalary
                    if ttsalary:
                        help = ttsalary + 1000000000000000
                        bhelp = int(a) + 10000000000
                        x.append(str(bhelp)[1:11] + str(help)[1:16] + b + '                             ' + '\n')
                        for i in tsalary:
                            i.status = "progress"
                            i.save()
                    # print(a)
                except Exception as e:
                    s = str(e)
                    print(s)
        # return render(request, 'output.txt', {'profile':profile}, content_type="text/plain")
        # messages.info(request, 'تمامی حواله ها ارسال شد')
        return x


class TransferFundCancel:
    def transferfundcancel(term, value, role):
        if role == 'institute':
            tsalary = TotalSalary.objects.filter(term=term, institute=value)
        else:
            tsalary = TotalSalary.objects.filter(term=term, user=value.user)
        for ts in tsalary:
            if ts.status != "True":
                ts.status = False
                ts.save()
        print('cancel')
