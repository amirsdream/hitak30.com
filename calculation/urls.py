from django.conf.urls import url, patterns
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    url(r'^calclist/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.calclist, name='calclist'),
    url(
        r'^transferfundcancelone/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/(?P<slug2>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/(?P<pk>\d+)/$',
        views.transferfundcancelone, name='transferfundcancelone'),
    url(
        r'^filter/(?P<slug>[a-z A-Z 1-9 ? \u0621-\u064A\u0660-\u0669]+)/(?P<slug2>[a-z A-Z 1-9 ? \u0621-\u064A\u0660-\u0669]+)/$',
        views.filter, name='filter'),
    url(
        r'^payconfirmone/(?P<slug>[a-z A-Z 1-9 ? \u0621-\u064A\u0660-\u0669]+)/(?P<slug2>[a-z A-Z 1-9 ? \u0621-\u064A\u0660-\u0669]+)/(?P<pk>\d+)/$',
        views.payconfirmone, name='payconfirmone'),
]

# from django.conf.urls import patterns, url

# urlpatterns += patterns('calculation.views',
#                         url(regex=r'^payajax/$',
#                             view='calclistajax',
#                             name='payajax'),
#                         )
