# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-06-29 15:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('calculation', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gpayments',
            name='profile',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='registeration.Profile'),
        ),
    ]
