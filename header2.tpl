<header id="header" class=" variant typeheader-<?php echo isset($typeheader) ? $typeheader : '1'?>">
	<div class="header-top compact-hidden">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-6 col-sm-8 header-top-left collapsed-block compact-hidden">
					<!-- LANGUAGE CURENTY -->
					<?php if($lang_status):?>
						<?php echo $currency; ?>
						<?php echo $language; ?>
					<?php endif; ?>

					<div class="tabBlocks form-group">
						<ul class="top-link list-inline">
							<li class="account" id="my_account"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="top-link-t btn-xs dropdown-toggle" data-toggle="dropdown"> <span ><i class="fa fa-user hidden-xs"></i><?php echo $text_account; ?></span> <span class="fa fa-caret-down"></span></a>
								<ul class="dropdown-menu ">
									<?php if ($logged) { ?>
									<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
									<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
									<li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
									<li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
									<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
									<?php } else { ?>
									<li><a href="<?php echo $register; ?>"><i class="fa fa-user"></i> <?php echo $text_register; ?></a></li>
									<li><a href="<?php echo $login; ?>"><i class="fa fa-pencil-square-o"></i> <?php echo $text_login; ?></a></li>
									<?php } ?>
								</ul>
							</li>

							<?php if($wishlist_status):?><li class="wishlist hidden-xs"><a href="<?php echo $wishlist; ?>" id="wishlist-total" class="top-link-t top-link-wishlist" title="<?php echo $text_wishlist; ?>"><span ><i class="fa fa-heart hidden-xs"></i><?php echo $text_wishlist; ?></span></a></li><?php endif; ?>

							<?php if($checkout_status):?><li class="checkout hidden-xs"><a href="<?php echo $checkout; ?>" class="top-link-t top-link-checkout" title="<?php echo $text_checkout; ?>"><span ><i class="fa fa-external-link hidden-xs"></i><?php echo $text_checkout; ?></span></a></li><?php endif; ?>
						</ul>
					</div>
				</div>
				<div class="col-lg-5 col-md-6 col-sm-4 hidden-xs header-top-right  form-inline">
					<div class="inner">
					<div class=" col-md-6 hidden-xs form-inline" align="center" >
					<div class"col-md-3 form-inline header-top-right" style="align><TABLE class=left cellSpacing=0 cellPadding=3 width="100%" border=0>
<TBODY><TR><TD style="MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px; WORD-SPACING: 0px">
<font size="2" face="Tahoma">
<SCRIPT LANGUAGE=JAVASCRIPT> 
function showdate() { 
    week= new Array("يكشنبه","دوشنبه","سه شنبه","چهارشنبه","پنج شنبه","جمعه","شنبه") 
    months = new Array("فروردين","ارديبهشت","خرداد","تير","مرداد","شهريور","مهر","آبان","آذر","دي","بهمن","اسفند"); 
    a = new Date(); 
    d= a.getDay(); 
    day= a.getDate(); 
    month = a.getMonth()+1; 
    year= a.getYear(); 
    year=(window.navigator.userAgent.indexOf('MSIE')>0)? year:1900+year;
    if (year== 0){year=2000;} 
    if (year<100){year +=1900;} 
    y=1; 
    for(i=0;i<3000;i+=4) { 
        if (year==i) {y=2;} 
        } 
    for(i=1;i<3000;i+=4) { 
        if (year==i) {y=3;} 
        } 
if (y==1) { 
        year -= ( (month < 3) || ((month == 3) && (day < 21)) )? 622:621; 
        switch (month) { 
            case 1: (day<21)? (month=10, day+=10):(month=11, day-=20); break; 
            case 2: (day<20)? (month=11, day+=11):(month=12, day-=19); break; 
            case 3: (day<21)? (month=12, day+=9):(month=1, day-=20);   break; 
            case 4: (day<21)? (month=1, day+=11):(month=2, day-=20);   break; 
            case 5: 
            case 6: (day<22)? (month-=3, day+=10):(month-=2, day-=21); break; 
            case 7: 
            case 8: 
            case 9: (day<23)? (month-=3, day+=9):(month-=2, day-=22);  break; 
            case 10:(day<23)? (month=7, day+=8):(month=8, day-=22);    break; 
            case 11: 
            case 12:(day<22)? (month-=3, day+=9):(month-=2, day-=21);  break; 
       default:          break; 
        } 
        } 
if (y==2) { 
        year -= ( (month < 3) || ((month == 3) && (day < 20)) )? 622:621; 
        switch (month) { 
            case 1: (day<21)? (month=10, day+=10):(month=11, day-=20); break; 
            case 2: (day<20)? (month=11, day+=11):(month=12, day-=19); break; 
            case 3: (day<20)? (month=12, day+=10):(month=1, day-=19);   break; 
            case 4: (day<20)? (month=1, day+=12):(month=2, day-=19);   break; 
            case 5: (day<21)? (month=2, day+=11):(month=3, day-=20);   break; 
            case 6: (day<21)? (month=3, day+=11):(month=4, day-=20); break; 
            case 7: (day<22)? (month=4, day+=10):(month=5, day-=21);   break; 
            case 8: (day<22)? (month=5, day+=10):(month=6, day-=21);   break; 
            case 9: (day<22)? (month=6, day+=10):(month=7, day-=21);  break; 
            case 10:(day<22)? (month=7, day+=9):(month=8, day-=21);    break; 
            case 11:(day<21)? (month=8, day+=10):(month=9, day-=20);   break; 
            case 12:(day<21)? (month=9, day+=10):(month=10, day-=20);  break; 
       default:          break; 
        } 
        } 
if (y==3) { 
        year -= ( (month < 3) || ((month == 3) && (day < 21)) )? 622:621; 
        switch (month) { 
            case 1: (day<20)? (month=10, day+=11):(month=11, day-=19); break; 
            case 2: (day<19)? (month=11, day+=12):(month=12, day-=18); break; 
            case 3: (day<21)? (month=12, day+=10):(month=1, day-=20);   break; 
            case 4: (day<21)? (month=1, day+=11):(month=2, day-=20);   break; 
            case 5: 
            case 6: (day<22)? (month-=3, day+=10):(month-=2, day-=21); break; 
            case 7: 
            case 8: 
            case 9: (day<23)? (month-=3, day+=9):(month-=2, day-=22);  break; 
            case 10:(day<23)? (month=7, day+=8):(month=8, day-=22);    break; 
            case 11: 
            case 12:(day<22)? (month-=3, day+=9):(month-=2, day-=21);  break; 
       default:          break; 
        } 
        } 
document.write(week[d]+" "+day+" "+months[month-1]+" "+ year); 
} 
</SCRIPT> 
      </FONT>
      </font>
      <P align=center><FONT color=#0033cc size=2 face="Tahoma">
      <SCRIPT>showdate()</SCRIPT>
      </FONT></P></TR></TBODY></TABLE></div>
						<div class="" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'fa', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
						</div>
        
						<div class="form-group navbar-welcome hidden-xs">
							<?php
								if (isset($welcome_message) && is_string($welcome_message)) {
									echo html_entity_decode($welcome_message, ENT_QUOTES, 'UTF-8');
								} else {echo 'Default welcome msg!';}
							?>
						


						</div>
						
						<?php echo '<div class="form-group navbar-phone hidden-sm hidden-xs"><i class="fa fa-phone"></i> '.html_entity_decode($contact_number, ENT_QUOTES, 'UTF-8').'</div>';
						?>
					
        
					
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- HEADER CENTER -->
	<div class="header-center">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 header-center-left">
					<?php //HEADER LOGO ?>
					<div class="header-logo">
					   <?php $this->soconfig->get_logo(); ?>
				    </div>
				    <?php //HEADER SEARCH ?>
				    <div class="header-search">
						<?php echo $content_search; ?>
					</div>
					<?php //HEADER CART ?>
					<div class="header-cart">
						<div class="shopping_cart pull-right">
						   <?php echo $cart; ?>
					    </div>
							
				</div>
			</div>
		</div>
	</div>
	
	<!-- HEADER BOTTOM -->
	<div class="header-bottom">
		<div class="container">
			<?php if ($content_menu): ?>
			<div class="header-navigation">
			   <?php echo $content_menu; ?>
			</div>
			<?php endif ?>
		</div>
	</div>
	
	<!-- Navbar switcher -->
	<?php if (!isset($toppanel_status) || $toppanel_status != 0) : ?>
	<?php if (!isset($toppanel_type) || $toppanel_type != 2 ) :  ?>
	<div class="navbar-switcher-container">
		<div class="navbar-switcher">
			<span class="i-inactive">
				<i class="fa fa-caret-down"></i>
			</span>
			 <span class="i-active fa fa-times"></span>
		</div>
	</div>
	<?php endif; ?>
	<?php endif; ?>
</header>
