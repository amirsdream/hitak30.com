from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

#
urlpatterns = [
    url(r'^driverlist/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.driverlist, name='driverlist'),
    url(r'^managerlist/$', views.managerlist, name='managerlist'),
    url(r'^managertermselect/$', views.managertermselect, name='managertermselect'),
    url(r'^servicemanagertermselect/$', views.servicemanagertermselect, name='servicemanagertermselect'),
    url(r'^selecttermins/$', views.selecttermins, name='selecttermins'),
    url(r'^selecttermdri/(?P<pk>\d+)/$', views.selecttermdri, name='selecttermdri'),
    url(r'^accounting/$', views.accounting, name='accounting'),
    url(r'^regdriver/$', views.regdriver, name='regdriver'),
    url(r'^driverindex/$', views.driverindex, name='driverindex'),
    url(r'^deletedriversystem/$', views.deletedriversystem, name='deletedriversystem'),
    url(r'^historydri/(?P<pk>\d+)/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.historydri,
        name='historydri'),
    url(r'^studentuserlist/$', views.studentuserlist, name='studentuserlist'),
    url(r'^studentuserlistterm/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.studentuserlistterm,
        name='studentuserlistterm'),
    url(r'^instituteaccountinglist/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.instituteaccountinglist,
        name='instituteaccountinglist'),
    url(r'^studentlist/(?P<pk>\d+)/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.studentlist,
        name='studentlist'),
    url(r'^studentlistdri/(?P<pk>\d+)/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.studentlistdri,
        name='studentlistdri'),
    url(r'^driveraccountinglist/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.driveraccountinglist,
        name='driveraccountinglist'),
    url(r'^manageraccountinglist/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.manageraccountinglist,
        name='manageraccountinglist'),
    url(r'^servicemanageraccountinglist/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.servicemanageraccountinglist,
        name='servicemanageraccountinglist'),
    url(
        r'^deldriver/(?P<pk>\d+)/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/(?P<slug2>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$',
        views.deldriver, name='deldriver'),
    url(
        r'^deldriverterm/(?P<pk>\d+)/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$',
        views.deldriverterm, name='deldriverterm'),
    url(r'^driverprofile/(?P<pk>\d+)/$', views.driverprofile, name='driverprofile'),
    url(r'^driverprofiledri/(?P<pk>\d+)/$', views.driverprofiledri, name='driverprofiledri'),
    url(r'^regdriver2/(?P<pk>\d+)/$', views.regdriver2, name='regdriver2'),
    url(r'^regdriver3/(?P<pk>\d+)/$', views.regdriver3, name='regdriver3'),
    url(r'^regdriver2edit/(?P<pk>\d+)/$', views.regdriver2edit, name='regdriver2edit'),
    url(r'^regdriver3edit/(?P<pk>\d+)/$', views.regdriver3edit, name='regdriver3edit'),
    url(r'^driverprofilelist/(?P<pk>\d+)/$', views.driverprofilelist, name='driverprofilelist'),
    url(r'^account/(?P<pk>\d+)/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/(?P<type>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.account, name='account'),
    url(r'^accountfactor/(?P<pk>\d+)/(?P<slug>[a-z A-Z 1-9\u0621-\u064A\u0660-\u0669]+)/$', views.accountfactor, name='accountfactor'),

]
