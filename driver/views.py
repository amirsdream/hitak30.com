import django_excel as excel
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404
from .models import Accounting, TotalSalary
from institute.models import Term, DriverStudent, Institute
from registeration.models import Profile, Model1, Factor
from .forms import RegisterDriver, ProfileForm, UserForm, DriverBankForm, AutoBankForm, AccountForm
from manager.models import Manager, ManagerService, TermIns
from django.http import HttpResponseRedirect


# from .models import Accounting


# Create your views here.

@login_required()
def regdriver(request):
    try:
        if request.method == 'POST':
            form = RegisterDriver(request.POST)
            if form.is_valid():
                user = form.save()
                user.email = "asd@gmail.com"
                user.refresh_from_db()  # load the profile instance created by the signal
                # user.profile.melli = form.cleaned_data.get('melli')
                user.profile.phone = form.cleaned_data.get('phone')
                user.profile.ephone = form.cleaned_data.get('ephone')
                user.profile.fathername = form.cleaned_data.get('fathername')
                # user.profile.bankacc = form.cleaned_data.get('bankacc')
                # user.profile.bankaccnum = form.cleaned_data.get('bankaccnum')
                # user.profile.bankaccname = form.cleaned_data.get('bankaccname')
                user.profile.postalcode = form.cleaned_data.get('postalcode')
                user.profile.marriagestat = form.cleaned_data.get('marriagestat')
                user.profile.education = form.cleaned_data.get('education')
                user.profile.validation = form.cleaned_data.get('validation')
                user.profile.insurance = form.cleaned_data.get('insurance')

                user.profile.role = 'driver'
                user.profile.gender = form.cleaned_data.get('gender')
                user.profile.bankacc = form.cleaned_data.get('bankacc')
                user.profile.bankaccnum = form.cleaned_data.get('bankaccnum')
                user.profile.bankaccname = form.cleaned_data.get('bankaccname')
                user.profile.vasighe = form.cleaned_data.get('vasighe')
                user.profile.validation = form.cleaned_data.get('validation')
                user.profile.bankacc = form.cleaned_data.get('bankacc')
                user.profile.bankaccnum = form.cleaned_data.get('bankaccnum')
                user.profile.bankaccname = form.cleaned_data.get('bankaccname')
                user.profile.vasighe = form.cleaned_data.get('vasighe')
                user.profile.validation = form.cleaned_data.get('validation')
                # user.profile.driver = user.username
                print(user.username)
                user.save()
                return redirect('driverindex')

        else:
            form = RegisterDriver()
    except Exception as e:
        s = str(e)
        print(s)
    return render(request, 'registerdriver.html', {'form': form})


@login_required()
def regdriver2(request, pk):
    prof = get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        form = DriverBankForm(request.POST, instance=prof)
        if form.is_valid():
            user = form.save()
            user.profile.bankacc = form.cleaned_data.get('bankacc')
            user.profile.bankaccnum = form.cleaned_data.get('bankaccnum')
            user.profile.bankaccname = form.cleaned_data.get('bankaccname')
            user.profile.vasighe = form.cleaned_data.get('vasighe')
            user.profile.validation = form.cleaned_data.get('validation')
            return redirect('regdriver3', pk=user.pk)
    else:
        form = DriverBankForm(instance=prof)
    return render(request, 'registerdriver2.html', {'form': form})


@login_required()
def regdriver2edit(request, pk):
    prof = get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        form = DriverBankForm(request.POST, instance=prof)
        if form.is_valid():
            user = form.save()
            user.profile.bankacc = form.cleaned_data.get('bankacc')
            user.profile.bankaccnum = form.cleaned_data.get('bankaccnum')
            user.profile.bankaccname = form.cleaned_data.get('bankaccname')
            user.profile.vasighe = form.cleaned_data.get('vasighe')
            user.profile.validation = form.cleaned_data.get('validation')
            return redirect('driverprofilelist', pk=user.pk)
    else:
        form = DriverBankForm(instance=prof)
    return render(request, 'registerdriver2edit.html', {'form': form})


@login_required()
def regdriver3(request, pk):
    prof = get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        form = AutoBankForm(request.POST, instance=prof)
        if form.is_valid():
            user = form.save()
            user.profile.insurancenum = form.cleaned_data.get('insurancenum')
            user.profile.license = form.cleaned_data.get('license')
            user.profile.licensedate = form.cleaned_data.get('licensedate')
            user.profile.licensenum = form.cleaned_data.get('licensenum')
            user.profile.licenseduration = form.cleaned_data.get('licenseduration')
            user.profile.auttype = form.cleaned_data.get('auttype')
            user.profile.carnum = form.cleaned_data.get('carnum')
            user.profile.autmodel = form.cleaned_data.get('autmodel')
            user.profile.techcheck = form.cleaned_data.get('techcheck')
            user.profile.motornum = form.cleaned_data.get('motornum')
            user.profile.autcolor = form.cleaned_data.get('autcolor')
            user.profile.reltype = form.cleaned_data.get('reltype')
            user.profile.vasighe = form.cleaned_data.get('vasighe')
            user.profile.canceldate = form.cleaned_data.get('canceldate')
            user.profile.cancelcause = form.cleaned_data.get('cancelcause')
            user.profile.description = form.cleaned_data.get('description')
            return redirect('driverprofilelist', pk=pk)
    else:
        form = AutoBankForm(instance=prof)
    return render(request, 'registerdriver2.html', {'form': form})


@login_required()
def regdriver3edit(request, pk):
    prof = get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        form = AutoBankForm(request.POST, instance=prof)
        if form.is_valid():
            user = form.save()
            user.profile.insurancenum = form.cleaned_data.get('insurancenum')
            user.profile.license = form.cleaned_data.get('license')
            user.profile.licensedate = form.cleaned_data.get('licensedate')
            user.profile.licensenum = form.cleaned_data.get('licensenum')
            user.profile.licenseduration = form.cleaned_data.get('licenseduration')
            user.profile.auttype = form.cleaned_data.get('auttype')
            user.profile.carnum = form.cleaned_data.get('carnum')
            user.profile.autmodel = form.cleaned_data.get('autmodel')
            user.profile.techcheck = form.cleaned_data.get('techcheck')
            user.profile.motornum = form.cleaned_data.get('motornum')
            user.profile.autcolor = form.cleaned_data.get('autcolor')
            user.profile.reltype = form.cleaned_data.get('reltype')
            user.profile.vasighe = form.cleaned_data.get('vasighe')
            user.profile.canceldate = form.cleaned_data.get('canceldate')
            user.profile.cancelcause = form.cleaned_data.get('cancelcause')
            user.profile.description = form.cleaned_data.get('description')
            return redirect('driverprofilelist', pk=pk)
    else:
        form = AutoBankForm(instance=prof)
    return render(request, 'registerdriver3edit.html', {'form': form})


@login_required()
def driverlist(request, slug):
    term = Term.objects.filter(name=slug)
    print(term)
    profile1 = term[0].driver.all()
    profile = []
    for pro in profile1:
        profile.append(get_object_or_404(Profile, pk=pro.pk))
    print(profile)
    # profile = Profile.objects.filter(.Q(role='driver') | Q(role='manager'))
    return render(request, 'driverlist.html', {'profile': profile, 'slug': slug})


@login_required()
def driverprofilelist(request, pk):
    return render(request, 'selectprofile.html', {'pk': pk})


@login_required()
def managerlist(request):
    profile = Profile.objects.filter(role='manager')
    return render(request, 'showmanager.html', {'profile': profile})


@login_required()
def studentlist(request, pk, slug):
    driver = get_object_or_404(User, pk=pk)
    term = get_object_or_404(Term, name=slug)
    try:
        student = DriverStudent.objects.filter(term=term).filter(driver=driver)[0]
        student = student.students.all()
        print(student)
    except:
        student = []
    # student = Profile.objects.filter(driver=driver)

    return render(request, 'studentlist.html', {'students': student, 'slug': slug, 'slug2': driver.pk})


@login_required()
def studentuserlist(request):
    student = Profile.objects.filter(role='user1')
    return render(request, 'userlist.html', {'student': student})


@login_required()
def driverindex(request):
    return render(request, 'driverindex.html', {})


@login_required()
def driverprofile(request, pk):
    user1 = get_object_or_404(User, pk=pk)
    profile = get_object_or_404(Profile, pk=pk)
    if request.method == 'POST':
        form_set = ProfileForm(request.POST, instance=profile)
        form = UserForm(request.POST, instance=user1)
        if all([form.is_valid(), form_set.is_valid()]):
            user = form.save()
            # user.profile.melli = form.cleaned_data.get('melli')
            user.profile.phone = form.cleaned_data.get('phone')
            user.profile.ephone = form.cleaned_data.get('ephone')
            user.profile.fathername = form.cleaned_data.get('fathername')
            # user.profile.bankacc = form.cleaned_data.get('bankacc')
            # user.profile.bankaccnum = form.cleaned_data.get('bankaccnum')
            # user.profile.bankaccname = form.cleaned_data.get('bankaccname')
            user.profile.postalcode = form.cleaned_data.get('postalcode')
            user.profile.marriagestat = form.cleaned_data.get('marriagestat')
            user.profile.education = form.cleaned_data.get('education')
            user.profile.validation = form.cleaned_data.get('validation')
            user.profile.insurance = form.cleaned_data.get('insurance')
            user.profile.role = form.cleaned_data.get('role')
            user.profile.gender = form.cleaned_data.get('gender')
            user.profile.bankacc = form.cleaned_data.get('bankacc')
            user.profile.bankaccnum = form.cleaned_data.get('bankaccnum')
            user.profile.bankaccname = form.cleaned_data.get('bankaccname')
            user.profile.vasighe = form.cleaned_data.get('vasighe')
            user.profile.validation = form.cleaned_data.get('validation')
            user.profile.cancelcause = form.cleaned_data.get('cancelcause')
            user.profile.canceldate = form.cleaned_data.get('canceldate')
            user.save()
            poll2 = form_set.save()
            poll2.save()
            return redirect('driverindex')

    else:
        form_set = ProfileForm(instance=profile)
        form = UserForm(instance=user1)

    return render(request, 'driverprofile.html', {'form': form, 'form_set': form_set})


@login_required()
def driverprofiledri(request, pk):
    user1 = get_object_or_404(User, pk=pk)
    profile = get_object_or_404(Profile, pk=pk)
    if request.method == 'POST':
        form_set = ProfileForm(request.POST, instance=profile)
        form = UserForm(request.POST, instance=user1)
        if all([form.is_valid(), form_set.is_valid()]):
            user = form.save()
            # user.profile.melli = form.cleaned_data.get('melli')
            user.profile.phone = form.cleaned_data.get('phone')
            user.profile.ephone = form.cleaned_data.get('ephone')
            user.profile.fathername = form.cleaned_data.get('fathername')
            # user.profile.bankacc = form.cleaned_data.get('bankacc')
            # user.profile.bankaccnum = form.cleaned_data.get('bankaccnum')
            # user.profile.bankaccname = form.cleaned_data.get('bankaccname')
            user.profile.postalcode = form.cleaned_data.get('postalcode')
            user.profile.marriagestat = form.cleaned_data.get('marriagestat')
            user.profile.education = form.cleaned_data.get('education')
            user.profile.validation = form.cleaned_data.get('validation')
            user.profile.insurance = form.cleaned_data.get('insurance')

            user.profile.role = form.cleaned_data.get('role')
            user.profile.gender = form.cleaned_data.get('gender')
            user.profile.bankacc = form.cleaned_data.get('bankacc')
            user.profile.bankaccnum = form.cleaned_data.get('bankaccnum')
            user.profile.bankaccname = form.cleaned_data.get('bankaccname')
            user.profile.vasighe = form.cleaned_data.get('vasighe')
            user.profile.validation = form.cleaned_data.get('validation')
            user.profile.bankacc = form.cleaned_data.get('bankacc')
            user.profile.bankaccnum = form.cleaned_data.get('bankaccnum')
            user.profile.bankaccname = form.cleaned_data.get('bankaccname')
            user.profile.vasighe = form.cleaned_data.get('vasighe')
            user.profile.validation = form.cleaned_data.get('validation')
            user.save()
            poll2 = form_set.save()
            poll2.save()
            return redirect('main')

    else:
        form_set = ProfileForm(instance=profile)
        form = UserForm(instance=user1)

    return render(request, 'driverprofiledri.html', {'form': form, 'form_set': form_set})


@login_required()
def viewstudentroute(request, pk):
    user = get_object_or_404(User, )
    model1 = get_object_or_404(Model1, user=User)

    return render(request, 'studentlist.html', {'model1': model1})


@login_required()
def report_driver(request, pk):
    driver = get_object_or_404(User, pk=pk)
    student = Profile.objects.filter(driver=driver)
    j = 1
    for i in student:
        i.pk = j
        j += 1
    print(student)
    column_names = ['pk', 'user__username', 'user__first_name', 'user__last_name', 'user__phone']
    return excel.make_response_from_query_sets(
        student,
        column_names,
        'xlsx',
        file_name="result",
    )


@login_required()
def deldriver(request, pk, slug, slug2):
    user = get_object_or_404(User, pk=pk)
    term = get_object_or_404(Term, name=slug)
    driver = get_object_or_404(User, pk=slug2)
    DriverStudent.loose_student(driver, term, user)
    model = Model1.objects.filter(author=user).filter(term=term)
    for mod in model:
        mod.driver = None
        mod.save()

    return redirect('studentlist', pk=driver.pk, slug=slug)


@login_required()
def studentuserlistterm(request, slug):
    # student = Profile.objects.filter(role='user1')
    users = Model1.objects.filter(term__name=slug).filter(status=True).filter(cancel=False)
    term = Term.objects.filter(name=slug)
    student = []
    institute = []
    driver = []
    userchecker = []
    for usr in users:
        if usr.author not in userchecker:
            helper = get_object_or_404(Profile, user=usr.author)
            userchecker.append(usr.author)
            institute.append(usr.institute)
            driver.append(usr.driver)
            student.append(helper)

    student = student[::-1]
    institute = institute[::-1]
    driver = driver[::-1]
    student = list(zip(student, institute, driver))
    print(student)
    return render(request, 'userlist2.html', {'student': student, 'slug': slug})


@login_required()
def driveraccountinglist(request, slug):
    term = Term.objects.filter(name=slug)
    factor = Factor.objects.all()
    # print(term[0])
    profile = term[0].driver.all()
    price = 0
    stddriverar = []
    pricear = []
    statusar = []
    # print(int(factor[0].percent1))
    for pro in profile:
        try:
            # stddriver = DriverStudent.objects.filter(driver=pro).filter(term=term)[0]
            # print(stddriver.driver)
            factor1 = factor[0].percent1
            stddriverar.append(pro)
            price = calculator(term=term, pro=pro, factor=factor1)
            # helper = stddriver.students.all()
            # print(helper)
            #
            # price = 0
            # helperprice = 0
            # for stddrv in helper:
            #     helper2 = Model1.objects.filter(author=stddrv, term=term[0], status=True)
            #     for help in helper2:
            #         print(help.price)
            #         price += help.price
            #     # print(stddrv)
            #     # price += helperprice
            #     print(price)
            # price = (price * int(factor[0].percent1)) / 100
            tsalary, created = TotalSalary.objects.get_or_create(user=pro, term=term[0], role='driver')
            tsalary.basesalary = price
            accountdriver = Accounting.objects.filter(author=pro, term=term[0], role='driver')
            print(accountdriver)
            #
            for accdrv in accountdriver:
                if accdrv.account == "minus":
                    # price-=float(accdrv.subprice)
                    price = float(price) - float(accdrv.subprice)
                    print(price)
                    # pass
                else:
                    price = float(price) + float(accdrv.subprice)

            if tsalary.status=="False":
                statusar.append("پرداخت نشده")
                tsalary.user = pro
                tsalary.term = term[0]
                tsalary.totalsalary = price
                tsalary.save()
            elif tsalary.status == "progress":
                statusar.append("ارسال پرداخت")
                price = tsalary.totalsalary
            else:
                statusar.append("پرداخت شده")
                price = tsalary.totalsalary
            pricear.append(price)
        except Exception as e:
            s = str(e)
            print(s)
    profile = list(zip(stddriverar, pricear, statusar))

    return render(request, 'driveraccountinglist.html', {'price': price, 'profile': profile, 'slug': slug})


@login_required()
def instituteaccountinglist(request, slug):
    term = Term.objects.filter(name=slug)
    ins = Institute.objects.all()
    factor = Factor.objects.all()
    pricear = []
    insdrv = []
    statusar = []
    for pro in ins:
        price = 0
        insdrv.append(pro)
        print(pro)
        tsalary, created = TotalSalary.objects.get_or_create(institute=pro)

        model1 = Model1.objects.filter(institute=pro).filter(term=term[0]).filter(status=True)
        for helper in model1:
            print(helper.price)
            price += helper.price

        price = price * int(factor[0].factor2) / 100

        if tsalary.status=="False":
            statusar.append("پرداخت نشده")
            tsalary.institute = pro
            tsalary.totalsalary = price
            tsalary.term = term[0]
            tsalary.save()
        elif tsalary.status == "progress":
                statusar.append("ارسال پرداخت")
                price = tsalary.totalsalary
        else:
            statusar.append("پرداخت شده")
            price = tsalary.totalsalary
        pricear.append(price)
        # except:
        #     print('ok')

    profile = list(zip(insdrv, pricear, statusar))

    return render(request, 'instituteaccountinglist.html', {'price': price, 'profile': profile, 'slug': slug})


@login_required()
def manageraccountinglist(request, slug):
    term = Term.objects.filter(name=slug)
    factor = Factor.objects.all()
    # print(term[0])
    price = 0
    stddriverar = []
    pricear = []
    statusar = []
    try:
        profile = term[0].manager.all()
        for pro2 in profile:
            managermanager = get_object_or_404(Manager, manager=pro2)
            manserv = managermanager.managerservices.all()
            termins = TermIns.objects.filter(user=managermanager.manager, term=term[0])
            termins = termins[0].institute.all()

            stddriverar.append(pro2)
            factor1 = factor[0].factor3
            price = 0
            for pro1 in manserv:
                managerdrv = get_object_or_404(ManagerService, managerservice=pro1)
                mandrv = managerdrv.drivers.all()
                for pro in mandrv:
                    for i in termins:
                        try:
                            price += calculator2(term=term, pro=pro, factor=factor1, institute=i.name)



                        except:
                            print('error')
            tsalary, created = TotalSalary.objects.get_or_create(user=pro2, role='manager', term=term[0])
            tsalary.basesalary = price
            accountdriver = Accounting.objects.filter(author=pro2, term=term[0], role='manager')
            # print(pro)
            #
            print(str(price) + 'ok')
            for accdrv in accountdriver:
                if accdrv.account == "minus":
                    # price-=float(accdrv.subprice)
                    price = float(price) - float(accdrv.subprice)
                    # pass
                else:
                    price = float(price) + float(accdrv.subprice)

            if tsalary.status == "False":
                statusar.append("پرداخت نشده")
                tsalary.user = pro2
                tsalary.term = term[0]
                tsalary.totalsalary = price
                tsalary.save()
            elif tsalary.status == "progress":
                statusar.append("ارسال پرداخت")
                price = tsalary.totalsalary
            else:
                statusar.append("پرداخت شده")
                price = tsalary.totalsalary
            pricear.append(price)

    except Exception as e:
        s = str(e)
        print(s)

    profile = list(zip(stddriverar, pricear, statusar))
    return render(request, 'manageraccountinglist.html', {'price': price, 'profile': profile, 'slug': slug})


@login_required()
def servicemanageraccountinglist(request, slug):
    term = Term.objects.filter(name=slug)
    factor = Factor.objects.all()
    print(term[0])
    price = 0
    stddriverar = []
    pricear = []
    statusar = []
    try:
        profile = term[0].managerservice.all()
        for pro1 in profile:
            print(pro1)
            managerdrv = get_object_or_404(ManagerService, managerservice=pro1, term=term[0])
            termins = TermIns.objects.filter(user=managerdrv.managerservice, term=term[0])
            termins = termins[0].institute.all()
            mandrv = managerdrv.drivers.all()
            stddriverar.append(pro1)
            print(mandrv)
            price = 0
            for pro in mandrv:
                for i in termins:
                    try:
                        factor1 = factor[0].percent2
                        stddriverar.append(pro)
                        price += calculator2(term=term, pro=pro, factor=factor1, institute=i.name)

                        # stddriver = get_object_or_404(DriverStudent, driver=pro, term=term[0])
                        # # print(stddriver.driver)
                        #
                        # helper = stddriver.students.all()
                        # print(helper)
                        # price2 = 0
                        # helperprice = 0
                        # for stddrv in helper:
                        #     helper2 = Model1.objects.filter(author=stddrv, term=term[0], status=True)
                        #     for help in helper2:
                        #         price2 += help.price
                        #         # print(price2)
                        #         # print(stddrv)
                        # price = price + (price2 * int(factor[0].percent2) / 100)
                        # print('ok')
                        print(price)
                    except:
                        print('error')
            print(pro1)
            # salarydel = TotalSalary.objects.all()
            # for s in salarydel:
            #     s.delete()
            tsalary, created = TotalSalary.objects.get_or_create(user=pro1, term=term[0], role='manager service')
            print(tsalary)
            tsalary.basesalary = price
            print('ok')
            accountdriver = Accounting.objects.filter(author=pro1, term=term[0], role='manager service')
            # print(managerdrv.managerservice)
            for accdrv in accountdriver:
                if accdrv.account == "minus":
                    # price-=float(accdrv.subprice)
                    price = float(price) - float(accdrv.subprice)
                    # pass
                else:
                    price = float(price) + float(accdrv.subprice)
            if tsalary.status == "False":
                statusar.append("پرداخت نشده")
                tsalary.user = pro1
                tsalary.term = term[0]
                tsalary.totalsalary = price
                tsalary.save()
            elif tsalary.status == "progress":
                statusar.append("ارسال پرداخت")
                price = tsalary.totalsalary
            else:
                statusar.append("پرداخت شده")
                price = tsalary.totalsalary
            pricear.append(price)
    except Exception as e:
        s = str(e)
        print(s)

    profile = list(zip(stddriverar, pricear, statusar))

    return render(request, 'servicemanageraccountinglist.html', {'price': price, 'profile': profile, 'slug': slug})


@login_required()
def accounting(request):
    return render(request, 'accounting.html', {})


@login_required()
def selecttermins(request):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        return redirect('instituteaccountinglist', slug=b)

    return render(request, 'termselectins.html', {'term': term})


@login_required()
def selecttermdri(request, pk):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        return redirect('studentlistdri', slug=b, pk=pk)

    return render(request, 'termselectdri.html', {'term': term})


@login_required()
def studentlistdri(request, pk, slug):
    driver = get_object_or_404(User, pk=pk)
    term = get_object_or_404(Term, name=slug)
    try:
        student = DriverStudent.objects.filter(term=term).filter(driver=driver)[0]
        student = student.students.all()
        print(student)
    except:
        student = []
    # student = Profile.objects.filter(driver=driver)

    return render(request, 'studentlistdri.html', {'students': student, 'slug': slug, 'slug2': driver})


@login_required()
def historydri(request, pk, slug):
    user = get_object_or_404(User, pk=pk)
    model1 = Model1.objects.filter(author=user, status=True, term__name=slug)
    return render(request, 'historydri.html', {'history': model1})


@login_required()
def account(request, pk, slug, type):
    driver = get_object_or_404(User, pk=pk)
    term = get_object_or_404(Term, name=slug)
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            poll = form.save(commit=False)
            poll.author = driver
            poll.term = term
            if type == 'servicemanager':
                poll.role = 'manager service'
            elif type == 'manager':
                poll.role = 'manager'
            else:
                poll.role = 'driver'
            poll.save()
            return redirect(type + 'accountinglist', slug=slug)

    else:

        form = AccountForm()

    return render(request, 'account.html', {'form': form})


@login_required()
def deldriverterm(request, pk, slug):
    driver = get_object_or_404(User, pk=pk)
    term = get_object_or_404(Term, name=slug)
    term.driver.remove(driver)
    model1 = Model1.objects.filter(term=term).filter(driver=driver)
    for m in model1:
        try:
            DriverStudent.loose_student(driver, term, m.author)
        except:
            pass
        print(m)
        m.driver = None
        m.save()
    return redirect('driverlist', slug=slug)


@login_required()
def managertermselect(request):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        return redirect('managerlist1', slug=b)

    return render(request, 'managertermselect.html', {'term': term})


@login_required()
def servicemanagertermselect(request):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        return redirect('managerservicelist', slug=b)

    return render(request, 'managertermselect.html', {'term': term})


@login_required()
def deletedriversystem(request):
    term = Profile.objects.filter(role='driver').filter(disable=False)
    if request.method == 'POST':
        b = request.POST['my_options2']
        user = get_object_or_404(Profile, user__username=b)
        print(user)
        user.disable = True
        user.save()
        return redirect('driverindex', )

    return render(request, 'deletedriversystem.html', {'term': term})


@login_required()
def accountfactor(request, pk, slug):
    profile = get_object_or_404(Profile, pk=pk)
    term = get_object_or_404(Term, name=slug)
    acc = Accounting.objects.filter(author=profile.user, term=term)
    tsalary = TotalSalary.objects.filter(user=profile.user, term=term)
    total = 0
    for t in tsalary:
        total += t.totalsalary

    return render(request, 'accountfactor.html',
                  {'total': total, 'profile': profile, 'acc': acc, 'tsalary1': tsalary, 'term': term})


def calculator(term, factor, pro):
    stddriver = DriverStudent.objects.filter(driver=pro).filter(term=term)[0]
    helper = stddriver.students.all()
    price = 0
    for stddrv in helper:
        helper2 = Model1.objects.filter(author=stddrv, term=term[0], status=True)
        for help in helper2:
            price += help.price
            print(price)
    print('ok')

    price = price * int(factor) / 100
    print(price)
    print('ok')
    return price


def calculator2(term, factor, pro, institute):
    stddriver = DriverStudent.objects.filter(driver=pro).filter(term=term)[0]
    helper = stddriver.students.all()
    price = 0
    institute = get_object_or_404(Institute, name=institute)
    for stddrv in helper:
        helper2 = Model1.objects.filter(author=stddrv, term=term[0], status=True, institute=institute)
        for help in helper2:
            price += help.price

    price = price * int(factor) / 100

    return price
