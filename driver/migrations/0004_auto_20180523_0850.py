# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-05-23 04:20
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_jalali.db.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('driver', '0003_accounting'),
    ]

    operations = [
        migrations.AddField(
            model_name='accounting',
            name='author',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='accounting',
            name='date',
            field=django_jalali.db.models.jDateField(default=datetime.datetime.now),
        ),
        migrations.AlterField(
            model_name='accounting',
            name='account',
            field=models.CharField(choices=[('moremoney', 'مساعده'), ('handymoney', 'پول دستی'), ('lastmoney', 'حقوق معوقه'), ('etc', 'غیره')], default='user1', max_length=100, null=True),
        ),
    ]
