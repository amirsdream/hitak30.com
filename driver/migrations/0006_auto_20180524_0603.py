# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-05-24 01:33
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0005_auto_20180523_1026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accounting',
            name='account',
            field=models.CharField(choices=[('minus', 'کسورات'), ('plus', 'اضافات')], default='user1', max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='accounting',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
