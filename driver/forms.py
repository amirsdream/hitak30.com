from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User  # fill in custom user info then save it

# from registration.models import Profile
from registeration.models import Profile
from .models import Accounting


class DriverBankForm(forms.ModelForm):
    bankacc = forms.CharField(max_length=30, required=False, help_text='Optional.')
    bankaccnum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    bankaccname = forms.CharField(max_length=30, required=False, help_text='Optional.')
    validation = forms.CharField(max_length=30, required=False, help_text='Optional.')
    vasighe = forms.CharField(max_length=30, required=False, help_text='Optional.')

    class Meta:
        model = Profile
        fields = ('bankacc', 'bankaccnum', 'bankaccname', 'validation', 'vasighe')

    def __init__(self, *args, **kwargs):
        super(DriverBankForm, self).__init__(*args, **kwargs)
        self.fields['bankacc'].label = "صاحب حساب بانکی"
        self.fields['bankaccnum'].label = "شماره حساب"
        self.fields['bankaccname'].label = "تام بانک صاحب حساب"
        self.fields['validation'].label = "اعتبارسنجی"
        self.fields['vasighe'].label = "وثیقه"


class AutoBankForm(forms.ModelForm):
    insurancenum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    license = forms.CharField(max_length=30, required=False, help_text='Optional.')
    licensedate = forms.CharField(max_length=30, required=False, help_text='Optional.')
    licensenum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    licenseduration = forms.CharField(max_length=30, required=False, help_text='Optional.')
    auttype = forms.CharField(max_length=30, required=False, help_text='Optional.')
    carnum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    autmodel = forms.CharField(max_length=30, required=False, help_text='Optional.')
    techcheck = forms.CharField(max_length=30, required=False, help_text='Optional.')
    motornum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    autcolor = forms.CharField(max_length=30, required=False, help_text='Optional.')
    reltype = forms.CharField(max_length=30, required=False, help_text='Optional.')

    class Meta:
        model = Profile
        fields = ('insurancenum', 'license', 'licensedate', 'licensenum', 'licenseduration',
                  'auttype', 'carnum', 'autmodel', 'techcheck', 'motornum', 'autcolor', 'reltype')

    def __init__(self, *args, **kwargs):
        super(AutoBankForm, self).__init__(*args, **kwargs)
        # self.fields['insurancenum'].label = "شماره بیمه"
        self.fields['license'].label = "گواهینامه"
        self.fields['licensedate'].label = "تاریخ گواهینامه"
        self.fields['licensenum'].label = "شماره گواهینامه"
        self.fields['licenseduration'].label = "اعتبار گواهینامه"
        self.fields['auttype'].label = "نوع خودرو"
        self.fields['carnum'].label = "شماره خودرو"
        self.fields['autmodel'].label = "مدل خودرو"
        self.fields['techcheck'].label = "معاینه فنی"
        self.fields['motornum'].label = "شماره موتور"
        self.fields['autcolor'].label = "رنگ خودرو"
        self.fields['reltype'].label = "نوع ارتباط"


class RegisterDriver(UserCreationForm):
    genderType = (
        ('male', u'آقا'),
        ('female', u'خانم'),
    )
    marType = (
        ('single', u'مجرد'),
        ('married', u'متاهل'),
    )
    roleType = (
        ('driver', 'راننده'),
        ('manager', u'مدیر اجرایی'),
        ('manager service', u'مدیر سرویس')
    )
    eduType = (
        ('dip', 'دیپلم'),
        ('fogh', u'فوق دیپلم'),
        ('lis', u'لیسانس'),
        ('foghlis', u'فوق لیسانس'),
        ('dr', u'دکتری'),
        ('sikl', u'سیکل'),
        ('noedu', u'بی سواد'),

    )
    bankType = (
        ('mellat', 'ملت'),
        ('tejarat', u'تجارت'),
    )
    first_name = forms.CharField(max_length=30, required=True, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=True, help_text='Optional.')
    image2 = forms.ImageField(required=False)

    # email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    # melli = forms.DecimalField(required=True, help_text='Optional.')
    phone = forms.DecimalField(required=True, help_text='Optional.')
    ephone = forms.DecimalField(required=True, help_text='Optional.')
    postalcode = forms.CharField(max_length=30, required=True, help_text='Optional.')
    # gender = forms.CharField(max_length=30, help_text='Optional.')
    fathername = forms.CharField(max_length=30, required=False, help_text='Optional.')
    marriagestat = forms.ChoiceField(choices=marType)
    # marriagestat = forms.CharField(max_length=30, required=False, help_text='Optional.')
    education = forms.ChoiceField(choices=eduType)
    # education = forms.CharField(max_length=30, required=False, help_text='Optional.')
    insurance = forms.CharField(max_length=30, required=False, help_text='Optional.')
    gender = forms.ChoiceField(choices=genderType)
    # role = forms.ChoiceField(choices=roleType)


    bankacc = forms.CharField(max_length=30, required=False, help_text='Optional.')
    bankaccnum = forms.IntegerField( required=False, help_text='Optional.')
    bankaccname = forms.ChoiceField(choices=bankType)
    validation = forms.CharField(max_length=30, required=False, help_text='Optional.')
    vasighe = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # insurancenum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    license = forms.CharField(max_length=30, required=False, help_text='Optional.')
    licensedate = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # licensenum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    licenseduration = forms.CharField(max_length=30, required=False, help_text='Optional.')
    auttype = forms.CharField(max_length=30, required=False, help_text='Optional.')
    carnum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    autmodel = forms.CharField(max_length=30, required=False, help_text='Optional.')
    techcheck = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # motornum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    autcolor = forms.CharField(max_length=30, required=False, help_text='Optional.')

    # reltype = forms.CharField(max_length=30, required=False, help_text='Optional.')

    # canceldate = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # cancelcause = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # description = forms.CharField(max_length=30, required=False, help_text='Optional.')

    class Meta:
        model = User
        fields = ('username', 'last_name', 'first_name', 'password1', 'password2', 'phone', 'ephone',)

        labels = {
            'username': u'کد ملی',
            'email': u'ایمیل',
            'password1': u'رمز',
            'password2': u'تکرار رمز',
            'First name': u'نام',
            'last_name': u'نام خانوادگی',
            'melli': u'کد ملی',
            'phone': u'شماره تماس',
            'ephone': u'شماره تماس اضطراری',
            'gender': u'جنسیت'
        }

    def __init__(self, *args, **kwargs):
        super(RegisterDriver, self).__init__(*args, **kwargs)
        # self.fields['email'].label = "ایمیل"
        self.fields['first_name'].label = "نام"
        self.fields['last_name'].label = "نام خانوادگی"
        self.fields['password1'].label = "رمز"
        self.fields['password2'].label = "تکرار رمز"
        # self.fields['melli'].label = "کد ملی"
        self.fields['phone'].label = "تلفن"
        self.fields['ephone'].label = "تلفن ضروری"
        self.fields['gender'].label = "جنسیت"
        self.fields['fathername'].label = "نام پدر"
        self.fields['postalcode'].label = "کد پستی"
        self.fields['marriagestat'].label = "وضعیت تاهل"
        self.fields['education'].label = "تحصیلات"
        self.fields['insurance'].label = "بیمه"
        # self.fields['canceldate'].label = "روز انصراف "
        # self.fields['cancelcause'].label = "دلبل انضزاف"
        # self.fields['description'].label = "توضیحات"
        # self.fields['role'].label = "نقش"
        self.fields['bankacc'].label = "صاحب حساب بانکی"
        self.fields['bankaccnum'].label = "شماره حساب"
        self.fields['image2'].label = "عکس"

        self.fields['bankaccname'].label = "تام بانک صاحب حساب"
        self.fields['validation'].label = "اعتبارسنجی"
        self.fields['vasighe'].label = "وثیقه"
        # self.fields['insurancenum'].label = "شماره بیمه"
        self.fields['license'].label = "گواهینامه"
        self.fields['licensedate'].label = "تاریخ گواهینامه"
        # self.fields['licensenum'].label = "شماره گواهینامه"
        self.fields['licenseduration'].label = "اعتبار گواهینامه"
        self.fields['auttype'].label = "نوع خودرو"
        self.fields['carnum'].label = "شماره خودرو"
        self.fields['autmodel'].label = "مدل خودرو"
        self.fields['techcheck'].label = "معاینه فنی"
        # self.fields['motornum'].label = "شماره موتور"
        self.fields['autcolor'].label = "رنگ خودرو"
        # self.fields['reltype'].label = "نوع ارتباط"

    class ProfileForm(forms.ModelForm):
        class Meta:
            model = Profile
            fields = '__all__'


class EditDriver(UserCreationForm):
    genderType = (
        ('male', u'آقا'),
        ('female', u'خانم'),
    )
    marType = (
        ('single', u'مجرد'),
        ('married', u'متاهل'),
    )
    roleType = (
        ('driver', 'راننده'),
        ('manager', u'مدیر اجرایی'),
        ('manager service', u'مدیر سرویس')
    )
    eduType = (
        ('dip', 'دیپلم'),
        ('fogh', u'فوق دیپلم'),
        ('lis', u'لیسانس'),
        ('foghlis', u'فوق لیسانس'),
        ('dr', u'دکتری'),
        ('sikl', u'سیکل'),
        ('noedu', u'بی سواد'),

    )
    bankType = (
        ('mellat', 'ملت'),
        ('tejarat', u'تجارت'),
    )
    first_name = forms.CharField(max_length=30, required=True, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=True, help_text='Optional.')
    # email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    melli = forms.DecimalField(required=True, help_text='Optional.')
    phone = forms.DecimalField(required=True, help_text='Optional.')
    ephone = forms.DecimalField(required=True, help_text='Optional.')
    postalcode = forms.CharField(max_length=30, required=True, help_text='Optional.')
    # gender = forms.CharField(max_length=30, help_text='Optional.')
    fathername = forms.CharField(max_length=30, required=False, help_text='Optional.')
    marriagestat = forms.ChoiceField(choices=marType)
    # marriagestat = forms.CharField(max_length=30, required=False, help_text='Optional.')
    education = forms.ChoiceField(choices=eduType)
    # education = forms.CharField(max_length=30, required=False, help_text='Optional.')
    insurance = forms.CharField(max_length=30, required=False, help_text='Optional.')
    gender = forms.ChoiceField(choices=genderType)
    # role = forms.ChoiceField(choices=roleType)
    image2 = forms.ImageField(required=False)

    bankacc = forms.CharField(max_length=30, required=False, help_text='Optional.')
    bankaccnum = forms.IntegerField( required=False, help_text='Optional.')
    bankaccname = forms.ChoiceField(choices=bankType)
    validation = forms.CharField(max_length=30, required=False, help_text='Optional.')
    vasighe = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # insurancenum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    license = forms.CharField(max_length=30, required=False, help_text='Optional.')
    licensedate = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # licensenum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    licenseduration = forms.CharField(max_length=30, required=False, help_text='Optional.')
    auttype = forms.CharField(max_length=30, required=False, help_text='Optional.')
    carnum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    autmodel = forms.CharField(max_length=30, required=False, help_text='Optional.')
    techcheck = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # motornum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    autcolor = forms.CharField(max_length=30, required=False, help_text='Optional.')

    # reltype = forms.CharField(max_length=30, required=False, help_text='Optional.')

    # canceldate = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # cancelcause = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # description = forms.CharField(max_length=30, required=False, help_text='Optional.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'melli', 'phone', 'ephone',)

        labels = {
            'username': u'کد ملی',
            'email': u'ایمیل',

            'First name': u'نام',
            'last_name': u'نام خانوادگی',
            'melli': u'کد ملی',
            'phone': u'شماره تماس',
            'ephone': u'شماره تماس اضطراری',
            'gender': u'جنسیت'
        }

    def __init__(self, *args, **kwargs):
        super(EditDriver, self).__init__(*args, **kwargs)
        # self.fields['email'].label = "ایمیل"
        self.fields['first_name'].label = "نام"
        self.fields['last_name'].label = "نام خانوادگی"
        self.fields['melli'].label = "کد ملی"
        self.fields['phone'].label = "تلفن"
        self.fields['ephone'].label = "تلفن ضروری"
        self.fields['gender'].label = "جنسیت"
        self.fields['fathername'].label = "نام پدر"
        self.fields['postalcode'].label = "کد پستی"
        self.fields['marriagestat'].label = "وضعیت تاهل"
        self.fields['education'].label = "تحصیلات"
        self.fields['insurance'].label = "بیمه"
        # self.fields['canceldate'].label = "روز انصراف "
        # self.fields['cancelcause'].label = "دلبل انضزاف"
        # self.fields['description'].label = "توضیحات"
        # self.fields['role'].label = "نقش"
        self.fields['bankacc'].label = "صاحب حساب بانکی"
        self.fields['bankaccnum'].label = "شماره حساب"
        self.fields['image2'].label = "عکس"

        self.fields['bankaccname'].label = "تام بانک صاحب حساب"
        self.fields['validation'].label = "اعتبارسنجی"
        self.fields['vasighe'].label = "وثیقه"
        # self.fields['insurancenum'].label = "شماره بیمه"
        self.fields['license'].label = "گواهینامه"
        self.fields['licensedate'].label = "تاریخ گواهینامه"
        # self.fields['licensenum'].label = "شماره گواهینامه"
        self.fields['licenseduration'].label = "اعتبار گواهینامه"
        self.fields['auttype'].label = "نوع خودرو"
        self.fields['carnum'].label = "شماره خودرو"
        self.fields['autmodel'].label = "مدل خودرو"
        self.fields['techcheck'].label = "معاینه فنی"
        # self.fields['motornum'].label = "شماره موتور"
        self.fields['autcolor'].label = "رنگ خودرو"
        # self.fields['reltype'].label = "نوع ارتباط"

    class ProfileForm(forms.ModelForm):
        class Meta:
            model = Profile
            exclude = ('password1', 'password2')


class ProfileForm(forms.ModelForm):
    genderType = (
        ('male', u'آقا'),
        ('female', u'خانم'),
    )
    marType = (
        ('single', u'مجرد'),
        ('married', u'متاهل'),
    )
    roleType = (
        ('driver', 'راننده'),
        ('manager', u'مدیر اجرایی'),
        ('manager service', u'مدیر سرویس')
    )
    eduType = (
        ('dip', 'دیپلم'),
        ('fogh', u'فوق دیپلم'),
        ('lis', u'لیسانس'),
        ('foghlis', u'فوق لیسانس'),
        ('dr', u'دکتری'),
        ('sikl', u'سیکل'),
        ('noedu', u'بی سواد'),

    )
    bankType = (
        ('mellat', 'ملت'),
        ('tejarat', u'تجارت'),
    )
    # melli = forms.DecimalField(required=True, help_text='Optional.')
    # first_name = forms.CharField(max_length=30, required=True, help_text='Optional.')
    # last_name = forms.CharField(max_length=30, required=True, help_text='Optional.')
    # email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    # melli = forms.DecimalField(required=True, help_text='Optional.')
    image2 = forms.ImageField(required=False)
    phone = forms.DecimalField(required=True, help_text='Optional.')
    ephone = forms.DecimalField(required=True, help_text='Optional.')
    postalcode = forms.CharField(max_length=30, required=True, help_text='Optional.')
    # gender = forms.CharField(max_length=30, help_text='Optional.')
    fathername = forms.CharField(max_length=30, required=False, help_text='Optional.')
    marriagestat = forms.ChoiceField(choices=marType)
    # marriagestat = forms.CharField(max_length=30, required=False, help_text='Optional.')
    education = forms.ChoiceField(choices=eduType)
    # education = forms.CharField(max_length=30, required=False, help_text='Optional.')
    insurance = forms.CharField(max_length=30, required=False, help_text='Optional.')
    gender = forms.ChoiceField(choices=genderType)
    # role = forms.ChoiceField(choices=roleType)

    bankacc = forms.CharField(max_length=30, required=False, help_text='Optional.')
    bankaccnum = forms.IntegerField( required=False, help_text='Optional.')
    bankaccname = forms.ChoiceField(choices=bankType)
    validation = forms.CharField(max_length=30, required=False, help_text='Optional.')
    vasighe = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # insurancenum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    license = forms.CharField(max_length=30, required=False, help_text='Optional.')
    licensedate = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # licensenum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    licenseduration = forms.CharField(max_length=30, required=False, help_text='Optional.')
    auttype = forms.CharField(max_length=30, required=False, help_text='Optional.')
    carnum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    autmodel = forms.CharField(max_length=30, required=False, help_text='Optional.')
    techcheck = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # motornum = forms.CharField(max_length=30, required=False, help_text='Optional.')
    autcolor = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # reltype = forms.CharField(max_length=30, required=False, help_text='Optional.')

    canceldate = forms.CharField(max_length=30, required=False, help_text='Optional.')
    cancelcause = forms.CharField(max_length=30, required=False, help_text='Optional.')
    description = forms.CharField(max_length=30, required=False, help_text='Optional.')

    class Meta:
        model = Profile
        exclude = ("user", "driver", "insurancenum", "licensenum", "motornum"
                   , "reltype", "salary1", "melli","disable","role")

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        # self.fields['first_name'].label = "نام"
        # self.fields['user.last_name'].label = "نام خانوادگی"
        # self.fields['melli'].label = "کد ملی"
        self.fields['image2'].label = "عکس"
        self.fields['state'].label = "وضعیت"
        self.fields['phone'].label = "تلفن"
        self.fields['ephone'].label = "تلفن ضروری"
        self.fields['gender'].label = "جنسیت"
        self.fields['fathername'].label = "نام پدر"
        self.fields['postalcode'].label = "کد پستی"
        self.fields['marriagestat'].label = "وضعیت تاهل"
        self.fields['education'].label = "تحصیلات"
        self.fields['insurance'].label = "بیمه"
        self.fields['canceldate'].label = "روز انصراف "
        self.fields['canceldate'].widget.attrs.update({'class': 'locale-en example3'})
        self.fields['cancelcause'].label = "دلیل انصراف"
        self.fields['description'].label = "توضیحات"
        # self.fields['role'].label = "نقش"

        self.fields['bankacc'].label = "صاحب حساب بانکی"
        self.fields['bankaccnum'].label = "شماره حساب"

        self.fields['bankaccname'].label = "تام بانک صاحب حساب"
        self.fields['validation'].label = "اعتبارسنجی"
        self.fields['vasighe'].label = "وثیقه"
        # self.fields['insurancenum'].label = "شماره بیمه"
        self.fields['license'].label = "گواهینامه"
        self.fields['licensedate'].label = "تاریخ گواهینامه"
        # self.fields['licensenum'].label = "شماره گواهینامه"
        self.fields['licenseduration'].label = "اعتبار گواهینامه"
        self.fields['auttype'].label = "نوع خودرو"
        self.fields['carnum'].label = "شماره خودرو"
        self.fields['autmodel'].label = "مدل خودرو"
        self.fields['techcheck'].label = "معاینه فنی"
        # self.fields['motornum'].label = "شماره موتور"
        self.fields['autcolor'].label = "رنگ خودرو"
        # self.fields['reltype'].label = "نوع ارتباط"
        # self.fields['image2'].label = "عکس"


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'last_name', 'first_name', 'email')

        labels = {
            'username': u'کدملی',
            'email': u'ایمیل',
            'password1': u'رمز',
            'password2': u'تکرار رمز',
            'First name': u'نام',
            'last_name': u'نام خانوادگی',
            'melli': u'کد ملی',
            'phone': u'شماره تماس',
            'ephone': u'شماره تماس اضطراری',
            'gender': u'جنسیت'
        }


class AccountForm(forms.ModelForm):
    class Meta:
        model = Accounting
        fields = ('account', 'date','nameofauthor', 'subprice', 'desc')
        widgets = {'date': forms.TextInput(attrs={'class': 'locale-en example3'})}
        labels = {'account': u'نوع:',
                  'date': u'تاریخ',
                  'subprice': u' مبلغ به ریال',
                  'desc': u'توضیحات',
                  'nameofauthor':u'نام مدیر اجرایی وارد کننده'}
