from django.contrib.auth.models import User
from django.db import models

from institute.models import Term, Institute
from datetime import datetime
from django_jalali.db import models as jmodels

# Create your models here.

# classDate1 = (
#     ('shanbe', u'شنبه'),
#     ('yekshanbe', u'یکشنبه'),
#     ('doshanbe', u'دوشنبه'),
#     ('seshanbe', u'سه شنبه'),
#     ('chaharshanbe', u'چهارشنبه'),
#     ('panjshanbe', u'پنج شنبه '),
#     ('jome', u'جمعه'),
# )
#
# roleType = (
#     ('user1', u'کاربر'),
#     ('manager', u'مدیر'),
#     ('driver', 'راننده')
# )
#
#
# genderType = (
#     ('male', u'آقا'),
#     ('female', u'خانم'),
# )
#
# class DriverProfile(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#     phone = models.DecimalField(max_digits=11, decimal_places=0, null=True, blank=True)
#     ephone = models.DecimalField(max_digits=11, decimal_places=0, null=True, blank=True)
#     melli = models.DecimalField(max_digits=11, decimal_places=0, null=True, blank=True)
#     role = models.CharField(max_length=100, choices=roleType, default='user1')
#     gender = models.CharField(max_length=100, choices=genderType, default='male')
#     fathername = models.CharField(max_length=100, null=True)
#     bankacc = models.CharField(max_length=100, null=True)
#     bankaccnum = models.CharField(max_length=100, null=True)
#     bankaccname = models.CharField(max_length=100, null=True)
#     postalcode = models.CharField(max_length=100, null=True)
#     marriagestat = models.CharField(max_length=100, null=True)
#     education = models.CharField(max_length=100, null=True)
#     validation = models.CharField(max_length=100, null=True)
#     insurance = models.CharField(max_length=100, null=True)
#     insurancenum = models.CharField(max_length=100, null=True)
#     license = models.CharField(max_length=100, null=True)
#     licensedate = models.CharField(max_length=100, null=True)
#     licensenum = models.CharField(max_length=100, null=True)
#     licenseduration = models.CharField(max_length=100, null=True)
#     auttype = models.CharField(max_length=100, null=True)
#     carnum = models.CharField(max_length=100, null=True)
#     autmodel = models.CharField(max_length=100, null=True)
#     techcheck = models.CharField(max_length=100, null=True)
#     motornum = models.CharField(max_length=100, null=True)
#     autcolor = models.CharField(max_length=100, null=True)
#     reltype = models.CharField(max_length=100, null=True)
#     vasighe = models.CharField(max_length=100, null=True)
#     canceldate = models.CharField(max_length=100, null=True)
#     cancelcause = models.CharField(max_length=100, null=True)
#     description = models.CharField(max_length=100, null=True)
#     # role = models.CharField(max_length=100, choices=roleType, default='driver')
#     image2 = StdImageField(upload_to='profileimage', blank=True, null=True,
#                            variations={'thumbnail': (400, 400, True), 'thumbnail2': (200, 200, True), })
#
# @receiver(post_save, sender=User)
# def update_user_profile(sender, instance, created, **kwargs):
#     if created:
#         DriverProfile.objects.create(user=instance)
#     instance.profile.save()

accountingtype = (
    ('minus', u'کسورات'),
    # ('handymoney', u'پول دستی'),
    # ('lastmoney', u'حقوق معوقه'),
    ('plus', u'اضافات')
)


class Accounting(models.Model):
    account = models.CharField(max_length=100, choices=accountingtype, default='user1', null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    # author = models.ForeignKey('auth.User', default=1)
    date = models.CharField(max_length=20, blank=True, null=True)
    nameofauthor = models.CharField(max_length=20, blank=True, null=True)
    subprice = models.DecimalField(max_digits=11, decimal_places=0, default=0)
    # studentnum = models.DecimalField(max_digits=11, decimal_places=0, null=True, blank=True)
    desc = models.TextField(blank=True, null=True)
    term = models.ForeignKey(Term, on_delete=models.CASCADE, blank=True, null=True)
    role = models.CharField(max_length=100, default='driver')


class TotalSalary(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    basesalary = models.DecimalField(max_digits=11, decimal_places=0, default=0)
    totalsalary = models.DecimalField(max_digits=11, decimal_places=0, default=0)
    term = models.ForeignKey(Term, on_delete=models.CASCADE, blank=True, null=True)
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, blank=True, null=True)
    status = models.CharField(max_length=100, default='False')
    receipt = models.CharField(max_length=100, null=True)
    role = models.CharField(max_length=100, default='driver')
