from django.shortcuts import render
from registeration.models import Profile
from institute.models import Term, Institute
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from .models import ManagerService, Manager, TermIns
from django.contrib.auth.decorators import login_required
from driver.models import Accounting, TotalSalary


# Create your views here.
@login_required()
def managerservicelist(request, slug):
    profile = Profile.objects.filter(role='manager service')
    term = Term.objects.filter(name=slug)[0]
    termins = TermIns.objects.filter(term=term)
    prof = []
    terminst = []
    for pro in profile:
        # user = get_object_or_404(User,username=pro.user.username)
        term.managerservice.add(pro.user)
        term.save()
        prof.append(pro)
        c = True
        for i in termins:
            if pro.user == i.user and pro.role == i.role:
                terminst.append(i.institute.all())
                c = False
        if c:
            terminst.append(' ')
    profile = list(zip(prof, terminst))
    print(profile)

    return render(request, 'managerservicelist.html', {'profile': profile, 'slug': slug})


@login_required()
def managerservicedriverlist(request, slug, pk):
    # profile = Profile.objects.filter(role='manager service')
    term = get_object_or_404(Term, name=slug)
    managerservice = get_object_or_404(User, pk=pk)
    profile = ManagerService.objects.filter(managerservice=managerservice)
    try:
        newprofile = profile[0].drivers.all()
    except:
        newprofile = None
        print(profile)

    return render(request, 'managerservicedriverlist.html', {'profile': newprofile, 'slug': slug, 'pk': pk})


@login_required()
def managerservicedriverlist2(request, slug, pk):
    # profile = Profile.objects.filter(role='manager service')
    term = get_object_or_404(Term, name=slug)
    managerservice = get_object_or_404(User, pk=pk)
    profile = ManagerService.objects.filter(managerservice=managerservice)
    try:
        newprofile = profile[0].drivers.all()
    except:
        newprofile = None
        print(profile)

    return render(request, 'managerservicedriverlist2.html', {'profile': newprofile, 'slug': slug, 'pk': pk})


@login_required()
def managerserviceadd(request, slug, pk):
    # driver = Profile.objects.filter(role='driver')
    managerservice = get_object_or_404(User, pk=pk)
    term = Term.objects.filter(status=True).filter(name=slug)
    driver = term[0].driver.all()
    if request.method == 'POST':
        c = request.POST['my_options3']
        print(c)
        u = User.objects.get(username=c)
        comp = get_object_or_404(Term, name=slug)
        ManagerService.make_drivers(managerservice, comp, u)

        # for pro in profile:
        #     u = User.objects.get(id=pro.id)
        #     print(term)
        #     comp.driver.add(u)
        #     comp.save()
        return redirect('managerservicedriverlist', slug=slug, pk=pk)

    return render(request, 'managerservicedriver.html', {'driver': driver, 'slug': slug, 'pk': pk})


@login_required()
def managerservicedel(request, slug, pk, pk2):
    managerservice = get_object_or_404(User, pk=pk)
    u = User.objects.get(pk=pk2)
    comp = get_object_or_404(Term, name=slug)
    ManagerService.loose_drivers(managerservice, comp, u)
    return redirect('managerservicedriverlist', slug=slug, pk=pk)


@login_required()
def managerlist(request, slug):
    profile = Profile.objects.filter(role='manager')
    term = Term.objects.filter(name=slug)[0]
    termins = TermIns.objects.filter(term=term)
    prof = []
    terminst = []
    for pro in profile:
        # user = get_object_or_404(User,username=pro.user.username)
        term.manager.add(pro.user)
        term.save()
        prof.append(pro)
        c = True
        for i in termins:
            if pro.user == i.user and pro.role == i.role:
                terminst.append(i.institute.all())
                c = False
        if c:
            terminst.append(' ')
    profile = list(zip(prof, terminst))
    print(profile)
    return render(request, 'managerlist1.html', {'profile': profile, 'slug': slug})


@login_required()
def manageraddlist(request, slug):
    term = Term.objects.filter(name=slug)[0]
    profile = term.driver.all()
    if request.method == 'POST':
        c = request.POST['my_options3']
        print(c)
        u = User.objects.get(username=c)
        p = Profile.objects.get(user__username=c)
        comp = get_object_or_404(Term, name=slug)
        print('ok')
        term.manager.add(u)
        term.save()
        p.role = "manager"
        p.save()
        return redirect('managerlist1', slug=slug)
    return render(request, 'manageradd.html', {'driver': profile, })


@login_required()
def managerserviceaddlist(request, slug):
    term = Term.objects.filter(name=slug)[0]
    profile = term.driver.all()
    if request.method == 'POST':
        c = request.POST['my_options3']
        print(c)
        u = User.objects.get(username=c)
        p = Profile.objects.get(user__username=c)
        comp = get_object_or_404(Term, name=slug)
        print('ok')
        term.managerservice.add(u)
        term.save()
        p.role = "manager service"
        p.save()
        return redirect('managerservicelist', slug=slug)
    return render(request, 'managerserviceadd.html', {'driver': profile, })


@login_required()
def managermanagerlist(request, slug, pk):
    # profile = Profile.objects.filter(role='manager service')
    term = get_object_or_404(Term, name=slug)
    manager = get_object_or_404(User, pk=pk)
    profile = Manager.objects.filter(manager=manager)
    try:
        newprofile = profile[0].managerservices.all()
    except:
        newprofile = None
        print(profile)
    return render(request, 'managerdriverlist.html', {'profile': newprofile, 'slug': slug, 'pk': pk})


# # Create your views here.
# @login_required()
# def managerservicelist(request, slug):
#     profile = Profile.objects.filter(role='manager service')
#     term = Term.objects.filter(name=slug)
#     for pro in profile:
#         term.managerservice.add(pro)
#         term.save()
#     return render(request, 'managerservicelist.html', {'profile': profile, 'slug': slug})

@login_required()
def managerservicedriverlist(request, slug, pk):
    # profile = Profile.objects.filter(role='manager service')
    term = get_object_or_404(Term, name=slug)
    managerservice = get_object_or_404(User, pk=pk)
    profile = ManagerService.objects.filter(managerservice=managerservice)
    try:
        newprofile = profile[0].drivers.all()
    except:
        newprofile = None
        print(profile)

    return render(request, 'managerservicedriverlist.html', {'profile': newprofile, 'slug': slug, 'pk': pk})


@login_required()
def managerservicedriverlist2(request, slug, pk):
    # profile = Profile.objects.filter(role='manager service')
    term = get_object_or_404(Term, name=slug)
    managerservice = get_object_or_404(User, pk=pk)
    profile = ManagerService.objects.filter(managerservice=managerservice)
    try:
        newprofile = profile[0].drivers.all()
    except:
        newprofile = None
        print(profile)

    return render(request, 'managerservicedriverlist2.html', {'profile': newprofile, 'slug': slug, 'pk': pk})


# @login_required()
# def managerserviceadd(request, slug, pk):
#     driver = Profile.objects.filter(role='driver')
#     managerservice = get_object_or_404(User, pk=pk)
#     # term = Term.objects.filter(status=True).filter(name=slug)
#     if request.method == 'POST':
#         c = request.POST['my_options3']
#         print(c)
#         u = User.objects.get(username=c)
#         comp = get_object_or_404(Term, name=slug)
#         ManagerService.make_drivers(managerservice, comp, u)
#
#         # for pro in profile:
#         #     u = User.objects.get(id=pro.id)
#         #     print(term)
#         #     comp.driver.add(u)
#         #     comp.save()
#         return redirect('managerservicedriverlist', slug=slug, pk=pk)
#
#     return render(request, 'managerservicedriver.html', {'driver': driver, 'slug': slug, 'pk': pk})
#

# @login_required()
# def managerservicedel(request, slug, pk, pk2):
#     managerservice = get_object_or_404(User, pk=pk)
#     u = User.objects.get(pk=pk2)
#     comp = get_object_or_404(Term, name=slug)
#     ManagerService.loose_drivers(managerservice, comp, u)
#
#     return redirect('managerservicedriverlist', slug=slug, pk=pk)


# @login_required()
# def managerlist(request, slug):
#     profile = Profile.objects.filter(role='manager')
#     term = Term.objects.filter(name=slug)[0]
#     for pro in profile:
#         term.manager.add(pro.user)
#         term.save()
#     return render(request, 'managerlist1.html', {'profile': profile, 'slug': slug})


@login_required()
def managermanagerlist2(request, slug, pk):
    # profile = Profile.objects.filter(role='manager service')
    term = get_object_or_404(Term, name=slug)
    manager = get_object_or_404(User, pk=pk)
    profile = Manager.objects.filter(manager=manager)
    try:
        newprofile = profile[0].managerservices.all()
    except:
        newprofile = None
        print(profile)
    return render(request, 'managerdriverlist2.html', {'profile': newprofile, 'slug': slug, 'pk': pk})


@login_required()
def manageradd(request, slug, pk):
    driver = Profile.objects.filter(role='manager service')
    manager = get_object_or_404(User, pk=pk)
    # term = Term.objects.filter(status=True).filter(name=slug)
    if request.method == 'POST':
        c = request.POST['my_options3']
        print(c)
        u = User.objects.get(username=c)
        comp = get_object_or_404(Term, name=slug)
        Manager.make_managerservices(manager, comp, u)
        print('ok')
        # for pro in profile:
        #     u = User.objects.get(id=pro.id)
        #     print(term)
        #     comp.driver.add(u)
        #     comp.save()
        return redirect('managermanagerlist', slug=slug, pk=pk)

    return render(request, 'managerdriver.html', {'driver': driver, 'slug': slug, 'pk': pk})


@login_required()
def managerdel(request, slug, pk, pk2):
    manager = get_object_or_404(User, pk=pk)
    u = User.objects.get(pk=pk2)
    comp = get_object_or_404(Term, name=slug)
    Manager.loose_managerservices(manager, comp, u)

    return redirect('managermanagerlist', slug=slug, pk=pk)


@login_required()
def delmanagerservice(request, slug, pk):
    # u = User.objects.get(pk=pk)
    profile = Profile.objects.filter(user__pk=pk)[0]
    term = Term.objects.filter(name=slug)[0]
    # user = get_object_or_404(profile.user)
    term.managerservice.remove(profile.user)
    profile.role = 'driver'
    tsalary = TotalSalary.objects.filter(user=profile.user, term=term, role="manager service")
    for i in tsalary:
        print('tsalary deleted')
        i.delete()
    # user = get_object_or_404(User, username=profile.user.username)
    # user.delete()
    profile.save()
    return redirect('managerservicelist', slug=slug)


@login_required()
def delmanager(request, slug, pk):
    # u = User.objects.get(pk=pk)
    profile = Profile.objects.filter(user__pk=pk)[0]
    term = Term.objects.filter(name=slug)[0]
    # user = get_object_or_404(profile.user)
    term.manager.remove(profile.user)

    profile.role = 'driver'
    tsalary = TotalSalary.objects.filter(user=profile.user, term=term, role="manager")
    for i in tsalary:
        i.delete()
    # user = get_object_or_404(User, username=profile.user.username)
    # user.delete()
    profile.save()
    return redirect('managerlist1', slug=slug)


@login_required()
def selectmanagerservicedri(request, pk):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        return redirect('managerservicedriverlist2', slug=b, pk=pk)

    return render(request, 'termselectdri.html', {'term': term})


@login_required()
def selectmanagerdri(request, pk):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        return redirect('managermanagerlist2', slug=b, pk=pk)

    return render(request, 'termselectdri.html', {'term': term})


@login_required()
def selecttermfactor(request, pk):
    term = Term.objects.filter(status=True)
    if request.method == 'POST':
        b = request.POST['my_options2']
        return redirect('accountfactor', slug=b, pk=pk)

    return render(request, 'termselectdri.html', {'term': term})


@login_required()
def selectservicemanagerinst(request, pk, slug):
    term = Institute.objects.all()
    term1 = get_object_or_404(Term, name=slug)
    if request.method == 'POST':
        a = request.POST['my_options']
        b = get_object_or_404(User, pk=pk)
        c = get_object_or_404(Institute, name=a)
        termins, created = TermIns.objects.get_or_create(term=term1, user=b)
        termins.institute.add(c)
        termins.role = "manager service"
        termins.save()
        return redirect('managerservicelist', slug=slug)

    return render(request, 'selectservicemanagerinst.html', {'driver': term, })


@login_required()
def deleteservicemanagerinst(request, pk, pk2):
    user = get_object_or_404(User, pk=pk)
    termins = get_object_or_404(TermIns, user=user, role="manager service")
    t = termins.institute.filter(pk=pk2)[0]
    termins.institute.remove(t)

    return redirect(request.META.get('HTTP_REFERER'))


@login_required()
def deletemanagerinst(request, pk, pk2):
    user = get_object_or_404(User, pk=pk)
    termins = get_object_or_404(TermIns, user=user, role="manager")
    t = termins.institute.filter(pk=pk2)[0]
    termins.institute.remove(t)

    return redirect(request.META.get('HTTP_REFERER'))


@login_required()
def selectmanagerinst(request, pk, slug):
    term = Institute.objects.all()
    term1 = get_object_or_404(Term, name=slug)
    if request.method == 'POST':
        a = request.POST['my_options']
        b = get_object_or_404(User, pk=pk)
        c = get_object_or_404(Institute, name=a)
        termins, created = TermIns.objects.get_or_create(term=term1, user=b)
        termins.institute.add(c)
        termins.role = "manager"
        termins.save()
        return redirect('managerlist1', slug=slug)

    return render(request, 'selectservicemanagerinst.html', {'driver': term, })
