from django.db import models
from django.contrib.auth.models import User

from institute.models import Term,Institute
from datetime import datetime
from django_jalali.db import models as jmodels


# Create your models here.

class Manager(models.Model):
    managerservices = models.ManyToManyField(User, related_name='managerservices')
    manager = models.ForeignKey(User, related_name='manager', null=True)
    term = models.ForeignKey(Term, related_name='termmanger', null=True)

    @classmethod
    def make_managerservices(cls, manager, term, new_managerservice):
        manager, created = cls.objects.get_or_create(
            manager=manager,
            term=term,
        )
        manager.managerservices.add(new_managerservice)

    @classmethod
    def loose_managerservices(cls, manager, term, new_managerservice):
        manager, created = cls.objects.get_or_create(
            manager=manager,
            term=term,
        )
        manager.managerservices.remove(new_managerservice)


class ManagerService(models.Model):
    drivers = models.ManyToManyField(User, related_name='drivers')
    managerservice = models.ForeignKey(User, related_name='managerservice', null=True)
    term = models.ForeignKey(Term, related_name='termmangerservice', null=True)

    @classmethod
    def make_drivers(cls, managerservice, term, new_driver):
        managerservice, created = cls.objects.get_or_create(
            managerservice=managerservice,
            term=term,
        )
        managerservice.drivers.add(new_driver)

    @classmethod
    def loose_drivers(cls, managerservice, term, new_driver):
        managerservice, created = cls.objects.get_or_create(
            managerservice=managerservice,
            term=term,
        )
        managerservice.drivers.remove(new_driver)


class TermIns(models.Model):
    institute = models.ManyToManyField(Institute,  related_name='institutes')
    user = models.ForeignKey(User, null=True)
    term = models.ForeignKey(Term, null=True)
    role = models.CharField(max_length=100, null=True,)
